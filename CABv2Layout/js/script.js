$(document).ready(function(e) {

	$('#cab-design .cab-control').each(function(index, element) {
    $(this).prepend('<div class="cab-control-actions"><span class="label label-default">#' + $(this).attr('id') + '</span> <span class="label label-primary cab-move"><i class="fa fa-arrows"></i> Move</span> <a href="#" class="label label-danger"><i class="fa fa-times"></i> Remove</a></div>')
  });

  $('#cab-design .cab-control').hover(
		function(e){
			e.stopImmediatePropagation();
			$(this).parents(".cab-control").removeClass('cab-active');
			$(this).addClass('cab-active');
		},
		function(e){
			e.stopImmediatePropagation();
			$(this).parents(".cab-control:first").addClass('cab-active');
			$(this).removeClass('cab-active');
		}
	);
	$('.cab-toggle-panel a').click(function(e) {
    $(this).children('i').toggleClass('fa-angle-left fa-angle-right');
  });
	$('.cab-toggle-panel.left a').click(function(e) {
    $('#cab-container').toggleClass('hide_left');
  });
	$('.cab-toggle-panel.right a').click(function(e) {
    $('#cab-container').toggleClass('hide_right');
  });

	$('.sortable').nestedSortable({
		handle: 'div',
		items: 'li',
		tolerance: 'intersect',
		cursor: 'move',
		protectRoot: true,
		grid: [ 0, 40 ]
	});

	 var editor = ace.edit("editor");
    editor.setTheme("ace/theme/monokai");
    editor.getSession().setMode("ace/mode/javascript");

});