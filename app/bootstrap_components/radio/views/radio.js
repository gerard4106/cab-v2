define(['sentientview', 'text!../templates/radio.hbs'], function (SentientView, template) {
	'use strict';

	var RadioView = SentientView.extend({

		template: template,

		init: function (options) {
			this.html(this.template);
			this.getControlProperties();
		},

		render: function () {
			var self = this;
			return self;
		},

		events: {
			'change #radio-inline' : 'onInlineChange'
		},

		//fill the property for the focued button
		getControlProperties: function () {
			if (this.checkActiveControl()) {

				if(this.focusedControl.find('label').hasClass('radio-inline')) {
					this.$el.find('#radio-inline').prop('checked', true);
				} else {
					this.$el.find('#radio-inline').prop('checked', false);
				}
			}
		},

		onInlineChange: function (e) {
			if (this.checkActiveControl()) {
				var isInlineEl = $(e.target).prop('checked');
				if (isInlineEl) {
					this.focusedControl.find('label').addClass('checkbox-inline');
				} else {
					this.focusedControl.find('label').removeClass('checkbox-inline');
				}
			}
		}

	});

	return RadioView;
});