define(['sentientview', 'text!../templates/buttongroup.hbs'], function (SentientView, template) {
	'use strict';

	var ButtonGroupView = SentientView.extend({

		template: template,

		init: function (options) {
			this.html(this.template);
			this.getControlProperties();
		},

		render: function () {
			var self = this;
			return self;
		},

		events: {
			'change #buttongroup-type' : 'onTypeChange',
			'change #buttongroup-size' : 'onSizeChange',
			'change #buttongroup-vertical' : 'onVerticalChange',
			'change #buttongroup-justified' : 'onJustifiedChange'
		},

		//fill the property for the focued button
		getControlProperties: function () {
			if (this.checkActiveControl()) {

				//get button type
				var typeClasses = this.sandbox.canvas.button.TYPE;
				for (var i = 0; i < typeClasses.length; i++) {
					if (this.focusedControl.find('.btn').hasClass(typeClasses[i])) {
						this.$el.find('#buttongroup-type').val(typeClasses[i]);
						break;
					}
				}

				//get button size
				var sizeClasses = this.sandbox.canvas.button.SIZE;
				for (var i = 0; i < sizeClasses.length; i++) {
					if (this.focusedControl.find('.btn').hasClass(sizeClasses[i])) {
						this.$el.find('#buttongroup-size').val(sizeClasses[i]);
						break;
					}
				}

				//determine if button is block el
				if(this.focusedControl.hasClass('btn-group-vertical')) {
					this.$el.find('#buttongroup-vertical').prop('checked', true);
				} else {
					this.$el.find('#buttongroup-vertical').prop('checked', false);
				}
			}
		},

		//NOTE:
		//changes events for button properties
		//this.checkActiveControl() class checks if control is valid or existing (declared in sentientView)
		//this.focusedControl contains the control itself (assigned in sentientView)
		//this.sandbox.canvas.ACTIVE contains the parent div of the control
		onTypeChange: function (e) {
			if (this.checkActiveControl()) {
				var classType = $(e.target).val();
				this.focusedControl.find('.btn').removeClass('btn-default btn-primary btn-success btn-warning btn-danger').addClass(classType);
			}
		},

		onSizeChange: function (e) {
			if (this.checkActiveControl()) {
				var classSize = $(e.target).val();
				this.focusedControl.find('.btn').removeClass('btn-lg btn-sm btn-md').addClass(classSize);
			}
		},

		onVerticalChange: function (e) {
			if (this.checkActiveControl()) {
				var isVerticalEl = $(e.target).prop('checked');
				if (isVerticalEl) {
					this.focusedControl.addClass('btn-group-vertical');
				} else {
					this.focusedControl.removeClass('btn-group-vertical');
				}
			}
		},

		onJustifiedChange: function (e) {
			if (this.checkActiveControl()) {
				var isJustifiedEl = $(e.target).prop('checked');
				if (isJustifiedEl) {
					this.focusedControl.addClass('btn-group-justified');
				} else {
					this.focusedControl.removeClass('btn-group-justified');
				}
			}
		}

	});

	return ButtonGroupView;
});