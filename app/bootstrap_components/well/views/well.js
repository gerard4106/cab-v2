define(['sentientview', 'text!../templates/well.hbs'], function (SentientView, template) {
	'use strict';

	var AppView = SentientView.extend({

		template: template,

		init: function () {
			this.$el.html(this.template);
		},

		render: function () {
			var self = this;
			self.getElementProperty();
			return self;
		},

		events: {
			'change #well-size': 'onChangeWellSize'
		},

		getElementProperty: function () {
			var self = this;
			if (self.checkActiveControl()) {
				if (self.focusedControl.hasClass('well-md')) {
					self.$el.find('#well-size').val('well-md');
				} else if (self.focusedControl.hasClass('well-sm')) {
					self.$el.find('#well-size').val('well-sm');
				} else if (self.focusedControl.hasClass('well-lg')) {
					self.$el.find('#well-size').val('well-lg');
				}
			}
		},

		onChangeWellSize: function (e) {
			var self = this;
			if (self.checkActiveControl()) {
				var val = $(e.target).val();
				self.focusedControl.removeClass('well-sm well-lg well-md');
				self.focusedControl.addClass(val);
			}
		}

	});

	return AppView;
});