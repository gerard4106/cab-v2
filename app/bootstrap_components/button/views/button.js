define(['sentientview', 'text!../templates/button.hbs'], function (SentientView, template) {
	'use strict';

	var ButtonView = SentientView.extend({

		template: template,

		init: function (options) {
			this.html(this.template);
			this.getControlProperties();
		},

		render: function () {
			var self = this;
			return self;
		},

		events: {
			'change #button-type' : 'onTypeChange',
			'change #button-size' : 'onSizeChange',
			'change #button-block' : 'onBlockChange'
		},

		//fill the property for the focued button
		getControlProperties: function () {
			if (this.checkActiveControl()) {

				//get button type
				var typeClasses = this.sandbox.canvas.button.TYPE;
				for (var i = 0; i < typeClasses.length; i++) {
					if (this.focusedControl.hasClass(typeClasses[i])) {
						this.$el.find('#button-type').val(typeClasses[i]);
						break;
					}
				}

				//get button size
				var sizeClasses = this.sandbox.canvas.button.SIZE;
				for (var i = 0; i < sizeClasses.length; i++) {
					if (this.focusedControl.hasClass(sizeClasses[i])) {
						this.$el.find('#button-size').val(sizeClasses[i]);
						break;
					}
				}

				//determine if button is block el
				if(this.focusedControl.hasClass('btn-block')) {
					this.$el.find('#button-block').prop('checked', true);
				} else {
					this.$el.find('#button-block').prop('checked', false);
				}
			}
		},

		//NOTE:
		//changes events for button properties
		//this.checkActiveControl() class checks if control is valid or existing (declared in sentientView)
		//this.focusedControl contains the control itself (assigned in sentientView)
		//this.sandbox.canvas.ACTIVE contains the parent div of the control
		onTypeChange: function (e) {
			if (this.checkActiveControl()) {
				var classType = $(e.target).val();
				this.focusedControl.removeClass('btn-info btn-default btn-primary btn-success btn-warning btn-danger').addClass(classType);
			}
		},

		onSizeChange: function (e) {
			if (this.checkActiveControl()) {
				var classSize = $(e.target).val();
				this.focusedControl.removeClass('btn-lg btn-sm btn-md').addClass(classSize);
			}
		},

		onBlockChange: function (e) {
			if (this.checkActiveControl()) {
				var isBlockEl = $(e.target).prop('checked');
				if (isBlockEl) {
					this.focusedControl.addClass('btn-block');
				} else {
					this.focusedControl.removeClass('btn-block');
				}
			}
		}

	});

	return ButtonView;
});