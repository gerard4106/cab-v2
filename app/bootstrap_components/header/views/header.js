define(['sentientview', 'text!../templates/header.hbs'], function (SentientView, template) {
	'use strict';

	var AppView = SentientView.extend({

		template: template,

		init: function () {
			this.$el.html(this.template);
		},

		render: function () {
			var self = this;
			self.getElementProperty();
			return self;
		},

		events: {
			'change #header-size': 'onChangeHeaderSize'
		},

		getElementProperty: function () {
			var self = this;
			if (self.checkActiveControl()) {
				var tagName = self.focusedControl.prop('tagName').toLowerCase();
				self.$el.find('#header-size').val(tagName);
			}
		},

		onChangeHeaderSize: function (e) {
			var self = this;
			if (this.checkActiveControl()) {
				var val = $(e.target).val(),
					tagName = self.focusedControl.prop('tagName').toLowerCase();

				if (tagName !== val) {
					var newHeader = '<' + val + ' data-cab-control="header" class="cab-control"></' + val + '>',
						text = self.focusedControl.html();
					self.focusedControl.replaceWith($(newHeader).html(text));
				}

			}
		}

	});

	return AppView;
});