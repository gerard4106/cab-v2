/*
** Store all row presets
** Format as array
*/
'use strict';

define(function () {

  var templates = {
    1: ['<div class="col col-lg-12 row-dashed" sortable="true"></div>'].join(''),

    2: ['<div class="col col-lg-6 row-dashed" sortable="true"></div>',
      '<div class="col col-lg-6 row-dashed" sortable="true"></div>'].join(''),

    3: ['<div class="col col-lg-4 row-dashed" sortable="true"></div>',
      '<div class="col col-lg-4 row-dashed" sortable="true"></div>',
      '<div class="col col-lg-4 row-dashed" sortable="true"></div>'].join(''),

    4: ['<div class="col col-lg-4 row-dashed" sortable="true"></div>',
      '<div class="col col-lg-8 row-dashed" sortable="true"></div>'].join(''),

    5: ['<div class="col col-lg-8 row-dashed" sortable="true"></div>',
      '<div class="col col-lg-4 row-dashed" sortable="true"></div>'].join(''),

    6: ['<div class="col col-lg-3 row-dashed" sortable="true"></div>',
      '<div class="col col-lg-6 row-dashed" sortable="true"></div>',
      '<div class="col col-lg-3 row-dashed" sortable="true"></div>'].join(''),

    7: ['<div class="col col-lg-3 row-dashed" sortable="true"></div>',
      '<div class="col col-lg-3 row-dashed" sortable="true"></div>',
      '<div class="col col-lg-3 row-dashed" sortable="true"></div>',
      '<div class="col col-lg-3 row-dashed" sortable="true"></div>'].join('')
  };

  return templates;

});
