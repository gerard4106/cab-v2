define(['sentientview', 'text!../templates/row.hbs', './row-presets'], function (SentientView, template, rowPresets) {
	'use strict';

	var SelectView = SentientView.extend({

		template: template,

		init: function (options) {
			this.html(this.template);
		},

		render: function () {
			var self = this;
			return self;
		},

		events: {
			'change #row-col': 'onSelectRowType',
			'change #row-presets': 'onSelectPreset'
		},

		onSelectPreset: function (e) {
			var self = this;
			if (this.checkActiveControl()) {
				var val = $(e.target).val(),
					preset = $(rowPresets[val]),
					columns = self.focusedControl.children(),
					colType = self.focusedControl.attr('data-col-type');

				$(columns).each(function (i, col) {
					col = $(col);
					preset.html(col.html());
				});

				columns.remove();

				preset.each(function (i, col) {
					col = $(col);
					var colClass = col.attr('class').replace('col-lg', colType);
					col.attr('class', colClass);
					self.focusedControl.append(col);
					self.sandbox.emit('initialize-sortable-on-row', col);
				});

			}
		},

		onSelectRowType: function (e) {
			var self = this;
			if (this.checkActiveControl()) {
				var val = $(e.target).val(),
					colType = self.focusedControl.attr('data-col-type'),
					columns = self.focusedControl.children();

				$(columns).each(function (i, col) {
					col = $(col);
					var rowClass = col.attr('class');
					rowClass = rowClass.replace(colType, val);
					col.removeClass().addClass(rowClass);
				});

				self.focusedControl.attr('data-col-type', val);
			}
		}

	});

	return SelectView;
});