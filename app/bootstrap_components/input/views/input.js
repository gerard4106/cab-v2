define(['sentientview', 'text!../templates/input.hbs'], function (SentientView, template) {
	'use strict';

	var InputView = SentientView.extend({

		template: template,

		init: function (options) {
			this.html(this.template);
			this.getControlProperties();
		},

		render: function () {
			var self = this;
			return self;
		},

		events: {
			'change #input-size' : 'onSizeChange',
			'blur #input-placeholder' : 'onChangePlaceholder'
		},

		getControlProperties: function () {
			if (this.checkActiveControl()) {

				var sizeClasses = this.sandbox.canvas.button.SIZE;
				for (var i = 0; i < sizeClasses.length; i++) {
					if (this.focusedControl.hasClass(sizeClasses[i])) {
						this.$el.find('#input-size').val(sizeClasses[i]);
						break;
					}
				}

			}
		},

		onChangePlaceholder: function () {

		},

		onSizeChange: function (e) {
			if (this.checkActiveControl()) {
				var classSize = $(e.target).val();
				this.focusedControl.removeClass('input-lg input-sm input-md').addClass(classSize);
			}
		}

	});

	return InputView;
});