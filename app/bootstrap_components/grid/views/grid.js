define(['sentientview', 'text!../templates/grid.hbs'], function (SentientView, template) {
	'use strict';

	var GridView = SentientView.extend({

		template: template,

		init: function (options) {
			this.html(this.template);
			this.getControlProperties();
		},

		render: function () {
			var self = this;
			return self;
		},

		events: {
			'change #table-striped' : 'onStripedChange',
			'change #table-bordered' : 'onBorderedChange',
			'change #table-hover' : 'onHoverRowsChange',
			'change #table-condensed' : 'onCondensedChange',
			'change #table-responsive' : 'onResponsiveChange'
		},

		getControlProperties: function () {
			if (this.checkActiveControl()) {

				if(this.focusedControl.hasClass('table-striped')) {
					this.$el.find('#table-striped').prop('checked', true);
				} else {
					this.$el.find('#table-striped').prop('checked', false);
				}
				
				if(this.focusedControl.hasClass('table-bordered')) {
					this.$el.find('#table-bordered').prop('checked', true);
				} else {
					this.$el.find('#table-bordered').prop('checked', false);
				}
				
				if(this.focusedControl.hasClass('table-hover')) {
					this.$el.find('#table-hover').prop('checked', true);
				} else {
					this.$el.find('#table-hover').prop('checked', false);
				}
				
				if(this.focusedControl.hasClass('table-condensed')) {
					this.$el.find('#table-condensed').prop('checked', true);
				} else {
					this.$el.find('#table-condensed').prop('checked', false);
				}
				
				if(this.focusedControl.parent('div').hasClass('table-condensed')) {
					this.$el.find('#table-condensed').prop('checked', true);
				} else {
					this.$el.find('#table-condensed').prop('checked', false);
				}
			}
		},

		onStripedChange: function (e) {
			if (this.checkActiveControl()) {
				var isStripedEl = $(e.target).prop('checked');
				if (isStripedEl) {
					this.focusedControl.addClass('table-striped');
				} else {
					this.focusedControl.removeClass('table-striped');
				}
			}
		},
		
		onBorderedChange: function (e) {
			if (this.checkActiveControl()) {
				var isBorderedEl = $(e.target).prop('checked');
				if (isBorderedEl) {
					this.focusedControl.addClass('table-bordered');
				} else {
					this.focusedControl.removeClass('table-bordered');
				}
			}
		},
		
		onHoverRowsChange: function (e) {
			if (this.checkActiveControl()) {
				var isHoverRowsEl = $(e.target).prop('checked');
				if (isHoverRowsEl) {
					this.focusedControl.addClass('table-hover');
				} else {
					this.focusedControl.removeClass('table-hover');
				}
			}
		},
		
		onCondensedChange: function (e) {
			if (this.checkActiveControl()) {
				var isCondensedEl = $(e.target).prop('checked');
				if (isCondensedEl) {
					this.focusedControl.addClass('table-condensed');
				} else {
					this.focusedControl.removeClass('table-condensed');
				}
			}
		},
		
		onResponsiveChange: function (e) {
			if (this.checkActiveControl()) {
				var isResponsiveEl = $(e.target).prop('checked');
				if (isResponsiveEl) {
					this.focusedControl.parent('div').addClass('table-responsive');
				} else {
					this.focusedControl.parent('div').removeClass('table-responsive');
				}
			}
		}

	});

	return GridView;
});