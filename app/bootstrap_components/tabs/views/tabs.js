define(['sentientview', 'text!../templates/tabs.hbs'], function (SentientView, template) {
	'use strict';

	var TabView = SentientView.extend({

		template: template,

		init: function (options) {
			this.html(this.template);
			this.getControlProperties();
		},

		render: function () {
			var self = this;
			return self;
		},

		events: {

		},

		getControlProperties: function () {

		}

	});

	return TabView;
});