define(['sentientview', 'text!../templates/datasource.hbs'], function (SentientView, template) {
	'use strict';

	var DatasourceView = SentientView.extend({

		template: template,

		init: function (options) {
			this.html(this.template);

		},

		render: function () {
			var self = this;
			return self;
		},

		events: {

		}

	});

	return DatasourceView;
});