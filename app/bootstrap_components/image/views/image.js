define(['sentientview', 'text!../templates/image.hbs'], function (SentientView, template) {
	'use strict';

	var ButtonView = SentientView.extend({

		template: template,

		init: function (options) {
			this.html(this.template);
			this.getControlProperties();
		},

		render: function () {
			var self = this;
			return self;
		},

		events: {
			'change .imageStyle' : 'onStyleChange',
			'change #img-bordered' : 'onBorderedChange',
			'change #img-responsive' : 'onResponsiveChange'
		},

		//fill the property for the focued button
		getControlProperties: function () {
			
			if (this.checkActiveControl()) {
				
			}
			
		},

		onStyleChange: function (e) {
		},

		onBorderedChange: function (e) {
		},
		
		onResponsiveChange: function (e) {
		}

	});

	return ButtonView;
});