define(['sentientview', 'text!../templates/checkbox.hbs'], function (SentientView, template) {
	'use strict';

	var ChecboxView = SentientView.extend({

		template: template,

		init: function (options) {
			this.html(this.template);
			this.getControlProperties();
		},

		render: function () {
			var self = this;
			return self;
		},

		events: {
			'change #checkbox-inline' : 'onInlineChange'
		},

		//fill the property for the focued button
		getControlProperties: function () {
			if (this.checkActiveControl()) {

				if(this.focusedControl.find('label').hasClass('checkbox-inline')) {
					this.$el.find('#checkbox-inline').prop('checked', true);
				} else {
					this.$el.find('#checkbox-inline').prop('checked', false);
				}
			}
		},

		onInlineChange: function (e) {
			if (this.checkActiveControl()) {
				var isInlineEl = $(e.target).prop('checked');
				if (isInlineEl) {
					this.focusedControl.find('label').addClass('checkbox-inline');
				} else {
					this.focusedControl.find('label').removeClass('checkbox-inline');
				}
			}
		}

	});

	return ChecboxView;
});