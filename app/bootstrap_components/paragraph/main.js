define(['./views/paragraph'], function (AppView) {
  'use strict';

  return {

    initialize: function () {
      var app = new AppView({ sandbox: this.sandbox, html: this.html });
      this.html(app.render().el);
    }

  };

});