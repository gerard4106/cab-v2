define(['sentientview', 'text!../templates/paragraph.hbs'], function (SentientView, template) {
	'use strict';

	var ParagraphView = SentientView.extend({

		template: template,

		init: function (options) {
			this.html(this.template);
			this.getControlProperties();
		},

		render: function () {
			var self = this;
			return self;
		},

		events: {
			'change #paragraph-lead' : 'onLeadChange',
			'change #paragraph-color' : 'onColorChange',
			'change #paragraph-bgcolor' : 'onBGChange'
		},

		getControlProperties: function () {
			if (this.checkActiveControl()) {
				
				// ERROR PLEASE HELP
				/*var colorClasses = this.sandbox.canvas.paragraph.COLOR;
				for (var i = 0; i < colorClasses.length; i++) {
					if (this.focusedControl.hasClass(colorClasses[i])) {
						this.$el.find('#paragraph-color').val(colorClasses[i]);
						break;
					}
				}*/
				
				// ERROR PLEASE HELP
				/*var bgcolorClasses = this.sandbox.canvas.paragraph.BGCOLOR;
				for (var i = 0; i < bgcolorClasses.length; i++) {
					if (this.focusedControl.hasClass(bgcolorClasses[i])) {
						this.$el.find('#paragraph-bgcolor').val(bgcolorClasses[i]);
						break;
					}
				}*/

				if(this.focusedControl.hasClass('lead')) {
					this.$el.find('#paragraph-lead').prop('checked', true);
				} else {
					this.$el.find('#paragraph-lead').prop('checked', false);
				}
			}
		},
		
		onLeadChange: function (e) {
			if (this.checkActiveControl()) {
				var isLeadEl = $(e.target).prop('checked');
				if (isLeadEl) {
					this.focusedControl.addClass('lead');
				} else {
					this.focusedControl.removeClass('lead');
				}
			}
		},
		
		onColorChange: function (e) {
			if (this.checkActiveControl()) {
				var colorType = $(e.target).val();
				this.focusedControl.removeClass('text-default text-muted btn-success text-info text-warning text-danger').addClass(colorType);
			}
		},
		
		onBGChange: function (e) {
			if (this.checkActiveControl()) {
				var bgcolorType = $(e.target).val();
				this.focusedControl.removeClass('bg-default bg-muted btn-success bg-info bg-warning bg-danger').addClass(bgcolorType);
			}
		}

	});

	return ParagraphView;
});