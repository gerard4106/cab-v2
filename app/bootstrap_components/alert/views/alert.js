define(['sentientview', 'text!../templates/alert.hbs'], function (SentientView, template) {
	'use strict';

	var AlertView = SentientView.extend({

		template: template,

		init: function (options) {
			this.html(this.template);
			this.getControlProperties();
		},

		render: function () {
			var self = this;
			return self;
		},

		events: {
			'change #alert-type': 'onChangeType'
		},

		getControlProperties: function () {

		},

		onChangeType: function (e) {
			if (this.checkActiveControl()) {
				var val = $(e.target).val();
				console.log(this.focusedControl, val);
				this.focusedControl.removeClass('alert-default alert-info alert-primary alert-success alert-warning alert-danger');
				this.focusedControl.addClass(val);
			}
		}

	});

	return AlertView;
});