define(['sentientview', 'text!../templates/textarea.hbs'], function (SentientView, template) {
	'use strict';

	var TextareaView = SentientView.extend({

		template: template,

		init: function (options) {
			this.html(this.template);
			this.getControlProperties();
		},

		render: function () {
			var self = this;
			return self;
		},

		events: {
			'change #textarea-size' : 'onSizeChange'
		},

		getControlProperties: function () {
			if (this.checkActiveControl()) {

				var sizeClasses = this.sandbox.canvas.button.SIZE;
				for (var i = 0; i < sizeClasses.length; i++) {
					if (this.focusedControl.hasClass(sizeClasses[i])) {
						this.$el.find('#textarea-size').val(sizeClasses[i]);
						break;
					}
				}

			}
		},

		onSizeChange: function (e) {
			if (this.checkActiveControl()) {
				var classSize = $(e.target).val();
				this.focusedControl.removeClass('input-lg input-sm input-md').addClass(classSize);
			}
		}

	});

	return TextareaView;
});