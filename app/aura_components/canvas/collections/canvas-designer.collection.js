define(['backbone'], function (Backbone) {
  'use strict';

  var DesignerCollection = Backbone.Collection.extend({
    defaults: {}
  });

  return DesignerCollection;

});