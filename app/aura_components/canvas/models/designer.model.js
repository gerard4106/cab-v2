define(['backbone'], function (Backbone) {
  'use strict';

  var DesignerModel = Backbone.Model.extend({
    defaults: {},
    parse: function (response) {
      var designer = response.data;
      return designer;
    }
  });

  return DesignerModel;

});