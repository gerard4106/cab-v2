define(['./views/canvas'], function (CanvasView) {
  'use strict';

  return {

    initialize: function () {
      var canvas = new CanvasView({ sandbox: this.sandbox });
      this.html(canvas.render().el);
    }

  };

});