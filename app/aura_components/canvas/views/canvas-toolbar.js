define(['sentientview', 'text!../templates/canvas-toolbar.hbs'], function (SentientView, template) {
  'use strict';

  var AppView = SentientView.extend({

    template: template,

    init: function (options) {
      var self = this;
      self.sandbox = options.sandbox;
      self.focuseControl = options.control;
      self.$el.html(this.template);
    },

    render: function () {
      this.initializeColorpickers();
      return this;
    },

    events: {
      'click #toolbar-bold' : 'toolbarBold',
      'click #toolbar-italic' : 'toolbarItalic',
      'click #toolbar-underline' : 'toolbarUnderline',
      'click #toolbar-strikethrough' : 'toolbarStrikethrough',
      'click .toolbar-text-align' : 'toolbarTextAlign'
    },

    initializeColorpickers: function () {
      var self = this;

      self.$el.find('#toolbar-colorpicker-border').ColorPicker({
        onShow: function (colpkr) {
          $(colpkr).fadeIn(500);
          return false;
        },
        onHide: function (colpkr) {
          $(colpkr).fadeOut(500);
          return false;
        },
        onChange: function (hsb, hex) {
          $('.imgcolrpkr-border').css('background-color', '#' + hex);
          self.focuseControl.css('border', 'solid #' + hex);
        }
      });

      self.$el.find('#toolbar-colorpicker-font').ColorPicker({
        onShow: function (colpkr) {
          $(colpkr).fadeIn(400);
          return false;
        },
        onHide: function (colpkr) {
          $(colpkr).fadeOut(500);
          return false;
        },
        onChange: function (hsb, hex) {
          $('.imgcolrpkr-font').css('background-color', '#' + hex);
          self.focuseControl.css('color', '#' + hex);
        }
      });

      self.$el.find('#toolbar-colorpicker-background').ColorPicker({
        onShow: function (colpkr) {
          $(colpkr).fadeIn(500);
          return false;
        },
        onHide: function (colpkr) {
          $(colpkr).fadeOut(500);
          return false;
        },
        onChange: function (hsb, hex) {
          $('.imgcolrpkr-background').css('background-color', '#' + hex);
          self.focuseControl.css('background-color', '#' + hex);
        }
      });
    },

    getSelection: function () {
      var html = '';
      if (typeof window.getSelection !== 'undefined') {
        var sel = window.getSelection();
        if (sel.rangeCount) {
          var container = document.createElement('div');
          for (var i = 0, len = sel.rangeCount; i < len; ++i) {
            container.appendChild(sel.getRangeAt(i).cloneContents());
          }
          html = container.innerHTML;
        }
      } else if (typeof document.selection !== 'undefined') {
        if (document.selection.type === 'Text') {
          html = document.selection.createRange().htmlText;
        }
      }

      return html ? html : undefined;
    },

    toolbarBold: function (e) {
      e.preventDefault();
      var selection = this.getSelection(),
        control = this.focuseControl;

      if (selection) {
        var originalText = control.html(),
          str = /<strong>(\.*?)<\/strong>/;

        if (originalText.match(str)) {
          var text = $('<strong>').html(originalText).text();
          control.html(text);
        }

        else {
          var newText = '<strong>' + selection + '</strong>';
          originalText = originalText.replace(selection, newText);
          control.html(originalText);
        }

      }
    },

    toolbarItalic: function (e) {
      e.preventDefault();
      var selection = this.getSelection(),
        control = this.focuseControl;

      if (selection) {
        var originalText = control.html(),
          str = /<i>(\.*?)<\/i>/;

        if (originalText.match(str)) {
          var text = $('<i>').html(originalText).text();
          control.html(text);
        }

        else {
          var newText = '<i>' + selection + '</i>';
          originalText = originalText.replace(selection, newText);
          control.html(originalText);
        }

      }
    },

    toolbarUnderline: function (e) {
      e.preventDefault();
      var selection = this.getSelection(),
        control = this.focuseControl;

      if (selection) {
        var originalText = control.html(),
          str = /<u>(\.*?)<\/u>/;

        if (originalText.match(str)) {
          var text = $('<u>').html(originalText).text();
          control.html(text);
        }

        else {
          var newText = '<u>' + selection + '</u>';
          originalText = originalText.replace(selection, newText);
          control.html(originalText);
        }
      }
    },

    toolbarStrikethrough: function (e) {
      e.preventDefault();
      var selection = this.getSelection(),
        control = this.focuseControl;

      if (selection) {
        var originalText = control.html(),
          str = /<strike>(\.*?)<\/strike>/;

        if (originalText.match(str)) {
          var text = $('<strike>').html(originalText).text();
          control.html(text);
        }

        else {
          var newText = '<strike>' + selection + '</strike>';
          originalText = originalText.replace(selection, newText);
          control.html(originalText);
        }
      }
    },

    toolbarTextAlign: function (e) {
      e.preventDefault();
      var alignment = $(e.target).attr('data-align');
      if (typeof alignment === 'undefined') {
        alignment = $(e.target).parent().attr('data-align');
      }
      this.focuseControl.css('text-align', alignment);
    }

  });

  return AppView;

});
