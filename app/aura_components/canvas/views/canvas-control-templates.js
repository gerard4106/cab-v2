/*
** Store all system controls templates here
** Format as array
*/
'use strict';

define(function () {

  var templates = { // TODO: create
    wizard: [
      '<div class="cab-control" data-control=\"wizard\" cab-control=\"true\">',
        '<div class=\"progress cab-wizard cab-control\">',
          '<div class=\"progress-bar cab-wizard-step-container\" style=\"width: 33.33333333333333%">',
            '<div class=\"cab-wizard-step-label cab-wizard-step step-1 pull-left complete\">1</div>',
            '<div class=\"cab-wizard-step-label cab-wizard-step step-2 pull-right\">2</div>',
          '</div>',
          '<div class=\"progress-bar cab-wizard-step-container\" style=\"width: 33.33333333333333%\">',
            '<div class=\"cab-wizard-step-label cab-wizard-step step-3 pull-right\">3</div>',
          '</div>',
          '<div class="progress-bar cab-wizard-step-container" style="width: 33.33333333333333%">',
            '<div class="cab-wizard-step-label cab-wizard-step step-4  pull-right">4</div>',
          '</div>',
        '</div>',
        '<div class=\"cab-wizard-contents\">',
          '<div class=\"cab-wizard-content content-step-1\"></div>',
        '</div>',
      '</div>'
    ].join(''),

    header: [
      '<div data-control="header" cab-control="true">',
        '<h3 data-cab-control="header" class="cab-control">Heading</h3>',
      '</div>'
    ].join(''),

    badge: [
      '<div data-control="badge" cab-control="true">',
        '<span data-cab-control="badge" class="badge cab-control">',
          'Badge',
        '</span>',
      '</div>'
    ].join(''),

    paragraph: [
      '<div data-control="paragraph" cab-control="true">',
        '<p data-cab-control="paragraph" class="cab-control text-left">Nullam quis risus eget urna mollis ornare vel eu leo.</p>',
      '</div>'
    ].join(''),

    small: [
      '<div data-control="small" cab-control="true">',
        '<small data-cab-control="small" class="cab-control text-left">Nullam quis risus eget urna mollis ornare vel eu leo.</small>',
      '</div>'
    ].join(''),

    list: [
      '<div data-control="list" cab=control="true">',
        '<ul data-cab-control="list" class="cab-control">',
            '<li>Lorem ipsum dolor sit amet</li>',
            '<li>Nulla volutpat aliquam velit',
              '<ul>',
                '<li>Phasellus iaculis neque</li>',
                '<li>Purus sodales ultricies</li>',
              '</ul>',
            '</li>',
        '</ul>',
      '</div>'
    ].join(''),

    grid: [
      '<div data-control="grid" cab-control="true">',
        '<table class="table table-striped table-bordered table-hover cab-control" data-cab-control="grid">',
          '<thead>',
            '<tr>',
              '<th>Column One</th>',
              '<th>Column Two</th>',
              '<th>Column Three</th>',
            '</tr>',
          '</thead>',
          '<tbody>',
            '<tr>',
              '<td>data</td>',
              '<td>data</td>',
              '<td>data</td>',
            '</tr>',
          '</tbody>',
          '<tfoot>',
            '<tr>',
              '<th># of Records</th>',
              '<th># of Records</th>',
              '<th># of Records</th>',
            '</tr>',
          '</tfoot>',
        '</table>',
      '</div>'
    ].join(''),

    image: [
      '<div data-control="image" cab-control="true">',
        '<div class="row">',
            '<div class="col col-sm-2 model-thumbnail" style="min-width: 140px!important;">',
                '<div class="thumbnail">',
                    '<a href="#" data-toggle="modal">',
                        '<img class="img-thumbnail default cab-control" data-cab-control="image" src="img/img-140.png" style="background: url(data:image/gif;base64,) no-repeat left center;',
                        'background-size: cover;">',
                        '<div class="caption hidden">',
                            '<h4>$ 18</h4>',
                        '</div>',
                    '</a>',
                '</div>',
            '</div>',
          '</div>',
      '</div>'
    ].join(''),

    /*******************
    *** Form Cotnrols
    *******************/

    input: [
      '<div data-control="input" cab-control="true">',
        '<input data-cab-control="input" class="cab-control form-control" type="text" />',
      '</div>'
    ].join(''),

    textarea: [
      '<div data-control="textarea" cab-control="true">',
        '<textarea data-cab-control="textarea" class="cab-control form-control" rows="3"></textarea>',
      '</div>'
    ].join(''),

    checkbox: [
      '<div data-control="checkbox" cab-control="true">',
        '<div class="checkbox cab-control" data-cab-control="checkbox">',
          '<label>',
            '<input type="checkbox" value="">',
            'Option one is this and that&mdash;be sure to include why it\'s great',
          '</label>',
        '</div>',
     '</div>'
    ].join(''),

    radio: [
       '<div data-control="radio" cab-control="true">',
        '<div class="radio cab-control" data-cab-control="radio">',
          '<label>',
            '<input type="radio" checked="" value="option1" id="optionsRadios1" name="optionsRadios">',
            'Option one is this and that&mdash;be sure to include why it\'s great',
          '</label>',
        '</div>',
      '</div>'
    ].join(''),

    select: [
      '<div data-control="select" cab-control="true">',
        '<select data-cab-control="select" class="cab-control form-control">',
          '<option>Default</option>',
        '</select>',
      '</div>'
    ].join(''),

    country: [
      '<div data-control="country" cab-control="true">',
        '<select data-cab-control="country" class="cab-control form-control">',
          '<option>Country</option>',
        '</select>',
      '</div>'
    ].join(''),

    button: [
      '<div data-control="button" cab-control="true">',
        '<a href="#" data-cab-control="button" class="btn btn-default cab-control">Button</a>',
      '</div>'
    ].join(''),

    buttongroup: [
      '<div data-control="buttongroup" cab-control="true">',
        '<div class="btn-group row-dashed cab-control" sortable="true">',
          '<a href="#" data-cab-control="button" class="btn btn-default btn-sm cab-control">first</a>',
          '<a href="#" data-cab-control="button" class="btn btn-default btn-sm cab-control">second</a>',
          '<a href="#" data-cab-control="button" class="btn btn-default btn-sm cab-control">third</a>',
        '</div>',
      '</div>'
    ].join(''),

    boolean: [
      '<div data-control="boolean" cab-control="true">',
        '<div class="btn-group cab-control" sortable="true">',
          '<a href="#" class="btn btn-success btn-sm" value="true" checked>',
            '<i class="icon-ok"></i>',
          '</a>',
          '<a href="#" class="btn btn-default btn-sm" value="false">',
            '<i class="icon-remove"></i>',
          '</a>',
        '</div>',
      '</div>'
    ].join(''),

    /******************
    *** Layout Controls
    *******************/

    div: [
      '<div data-control="div" cab-control="true">',
        '<div data-cab-control="div" class="cab-control">',
        '</div>',
      '</div>'
    ].join(''),

    row: [
      '<div data-control="row" cab-control="true">',
        '<div data-cab-control="row" class="row cab-control" data-col-type="col-lg">',
          '<div class="col col-lg-12 row-dashed" sortable="true"></div>',
        '</div>',
      '</div>'
    ].join(''),

    tabs: [
      '<div data-control="tabs" cab-control="true">',
        '<div data-cab-control="tabs" class="cab-control">',
          '<ul class="nav nav-tabs tab-main-ul">',
            '<li class="active">',
            '<a href="#" class="cab-control" data-cab-control="button">nav</a>',
            '</li>',
            '<li class="">',
              '<a href="#" class="add-tab"><i class="icon-plus"></i></a>',
            '</li>',
          '</ul>',
          '<div class="tab-content">',
            '<div class="tab-pane row-dashed active" sortable="true">',
            '</div>',
          '</div>',
        '</div>',
      '</div>'
    ].join(''),

    well: [
      '<div data-control="well" cab-control="true">',
        '<div class="well cab-control well-md" data-cab-control="well" sortable="true"></div>',
      '</div>'
    ].join(''),

    panel: [
      '<div data-control="panel" cab-control="true">',
        '<div class="panel cab-control" data-cab-control="panel">',
          '<div class="panel-heading">Panel Title</div>',
          'Panel Content',
        '</div>',
      '</div>'
    ].join(''),

    spacer: [
      '<div data-control="spacer" cab-control="true">',
        '<div class="cab-control row-dashed" data-cab-control="spacer" style="height:inherit;"></div>',
      '</div>'
    ].join(''),

    /*****************
    *** Misc Controls
    *****************/

    label: [
      '<div data-control="label" cab-control="true">',
        '<label class="label cab-control" data-cab-control="label">Default</label>',
      '</div>'
    ].join(''),

    alert: [
      '<div data-control="alert" cab-control="true">',
        '<div class="alert alert-danger cab-control" data-cab-control="alert">',
          '<button data-dismiss="alert" class="close" type="button">×</button>',
          '<span>Your Default Message.</span>',
        '</div>',
      '</div>'
    ].join(''),

    progress: [
      '<div data-control="progress" cab-control="true">',
        '<div class="progress progress-striped active cab-control" data-cab-control="progress">',
          '<div style="width: 45%" class="progress-bar"></div>',
        '</div>',
      '</div>'
    ].join(''),

    chart: [
      '<div data-control="chart" cab-control="true">',
        '<article data-cab-control="chart" id="pieChart" class="cab-control">',
          '<div class="canvasWrapper">',
            '<canvas width="150" height="150"></canvas>',
          '</div>',
        '</article>',
      '</div>'
    ].join(''),

    accordion: [
      '<div data-control="accordion" cab-control="true">',
        '<div data-cab-control="accordion" class="panel-group cab-control" id="[parent]">',
          '<div class="panel panel-default">',
            '<div class="panel-heading">',
              '<a class="accordion-toggle" data-toggle="collapse" data-parent="[parent]" href="[group]">Default</a>',
            '</div>',
            '<div id="[group]" class="panel-collapse collapse in">',
              '<div class="panel-body" sortable="true">',
              '</div>',
            '</div>',
          '</div>',
        '</div>',
      '</div>'
    ].join(''),

    timeline: [
        '<div data-control="timeline" cab-control="true">',
          '<div class="row timeline-filter" data-cab-control="timeline">',
          '<div class="col col-2 col-lg-1 timeline-filter-year">',
          '<div class="input-group">',
          '<div class="input-group-btn">',
          '<button class="btn btn-primary btn-sm"><i class="icon-chevron-left"></i></button>',
          '</div>',
          '<input class="form-control input-sm" value="2013" type="text">',
          '<div class="input-group-btn">',
          '<button class="btn btn-primary btn-sm"><i class="icon-chevron-right"></i></button>',
          '</div>',
          '</div>',
          '</div>',
          '<div class="col col-10 col-lg-11 timeline-filter-month">',
          '<div class="row">',
          '<ul>',
          '<li class="col col-sm-1"><a href="#">Jan</a></li>',
          '<li class="col col-sm-1"><a href="#">Feb</a></li>',
          '<li class="col col-sm-1"><a href="#">Mar</a></li>',
          '<li class="col col-sm-1"><a href="#">Apr</a></li>',
          '<li class="col col-sm-1"><a href="#">May</a></li>',
          '<li class="col col-sm-1"><a href="#">Jun</a></li>',
          '<li class="col col-sm-1"><a href="#">Jul</a></li>',
          '<li class="col col-sm-1 active"><a href="#">Aug</a></li>',
          '<li class="col col-sm-1"><a href="#">Sep</a></li>',
          '<li class="col col-sm-1"><a href="#">Oct</a></li>',
          '<li class="col col-sm-1"><a href="#">Nov</a></li>',
          '<li class="col col-sm-1"><a href="#">Dec</a></li>',
          '</ul>',
          '</div>',
          '</div>',
          '</div>',
          '<div class="spacer" style="height:15px"></div>',
          '<div class="row cab-control">',
          '<div class="col col-sm-4 col-lg-5">',
          '<div class="popover left first">',
          '<div class="arrow"></div>',
          '<div class="nano"><div class="nano-content">',
          '<div class="popover-content" sortable="true">',
          '<!-- <p>Sed posuere consetcetur est at lobortis. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum.</p> -->',
          '</div>',
          '</div></div>',
          '</div>',
          '</div>',
          '<div class="col col-sm-4 col-lg-2">',
          '<div class="date">',
          '<div class="panel">',
          '<div class="panel-heading">TUE</div>',
          '<div class="panel-content">',
          '<h2>28</h2>',
          '<span>12:23P</span>',
          '</div>',
          '</div>',
          '</div>',
          '</div>',
          '<div class="col col-sm-4 col-lg-5">',
          '<div class="popover right">',
          '<div class="arrow"></div>',
          '<div class="nano"><div class="nano-content">',
          '<div class="popover-content timeline-onshow-fields">',
          '</div>',
          '</div></div>',
          '</div>',
          '</div>',
          '<div class="clearfix"></div>',
          '</div>',
        '</div>'
    ].join(''),


    carousel: [
       '<div data-control="carousel" cab-control="true">',
        '<div class="carousel ce-carousel slide cab-control" data-cab-control="carousel">',
          '<div class="carousel-inner">',
            '<div class="item active" data-item="1">',
                '<div class="row">',
                  '<div class="col-1"></div>',
                  '<div class="col col-10">',
                    '<div class="row parent-row">',
                      '<div class="col-lg-12 row-dashed carousel-row" sortable="true"></div>',
                    '</div>',
                  '</div>',
                  '<div class="col-1"></div>',
              '</div>',
            '</div>',
          '</div>',
          '<a class="left carousel-control" href="#carousel1" data-slide="prev"><span class="icon-chevron-sign-left icon-2x"></span></a>',
          '<a class="right carousel-control" href="#carousel1" data-slide="next"><span class="icon-chevron-sign-right icon-2x"></span></a>',
        '</div>',
      '</div>'
    ].join(''),

    custom: [
      '<div data-control="custom" cab-control="true">',
        '<div data-label="text" icon-label="false" class="cab-control" data-cab-control="custom">',
          '<label class="control-label">Label</label>',
          '<span data-bound-field="" name="" class="control-value">[ ControlValue ]</span>',
          '<div class="clearfix"></div>',
        '</div>',
      '</div>'
    ].join(''),

    repeater: [
      '<div data-control="repeater" cab-control="true">',
        '<div data-cab-control="repeater" class="row cab-control">',
          '<div class="col col-lg-12 row-dashed repeater" sortable="true"></div>',
        '</div>',
      '</div>'
    ].join(''),

    inputgroup: [
      '<div data-control="inputgroup" cab-control="true">',
        '<div class="input-group cab-control" data-cab-control="inputgroup">',
          '<input data-cab-control="input" class="cab-control form-control" type="text" />',
          '<span class="input-group-btn right">',
            '<a data-cab-control="button" class="btn btn-default cab-control">Button</a>',
          '</span>',
        '</div>',
      '</div>'
    ].join(''),

    customfield: [
      '<div data-control="customfield" cab-control="true">',
        '<div data-label="text" icon-label="false" class="cab-control" data-cab-control="custom">',
          '<label class="control-label">Label</label>',
          '<span class="control-value row-dashed" sortable="true">',
            '<!-- <input type="text" placeholder="shit" class="form-control input-sm" /> -->',
          '</span>',
          '<div class="clearfix"></div>',
        '</div>',
      '</div>'
    ].join(''),

    data_container: [
      '<div data-control="data_container" cab-control="true">',
        '<div class="row">',
          '<div class="col-sm-12 data-container-model">',
            '<div class="card-wrapper flip data-container cab-control row-dashed" data-cab-control="data-container" sortable="true" data-layout="1" data-grid-col="col-sm-12">',
              '<div class="card front ui-state-disabled">',
                '<a href="#" class="cardflip goflip ui-state-disabled">',
                  '<i class="card-icon icon-cog ui-state-disabled"></i>',
                '</a>',
              '</div>',
            '</div>',
          '</div>',
        '</div>',
      '</div>'
    ].join(''),

    button_dropdown: [
      '<div data-control="button_dropdown" cab-control="true">',
        '<div class="btn-group cab-control">',
          '<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">',
            'Action <span class="caret"></span>',
          '</button>',
          '<ul class="dropdown-menu" role="menu">',
          '</ul>',
        '</div>',
      '</div>'
    ].join('')

  }; // end template

  return templates;

});
