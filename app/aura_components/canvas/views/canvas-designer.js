define(['sentientview', './canvas-control-templates', './canvas-toolbar', '../models/designer.model', './canvas-control-templates', '../models/desinger-template.model'], function (SentientvView, ControlTemplates, ControlToolbarView, DesignerModel, controlTemplates, DesignerTplModel) {
  'use strict';

  var AppView = SentientvView.extend({

    preview: {
      isPreview: false,
      html: undefined
    },

    init: function () {
      this.$el.append(this.model.get('designerModel').contents);
      this.$el.attr('id', this.model.get('id'));
      this.$el.addClass('tab-pane active design-view');
      this.$el.data('widgetData', this.model.get('data'));

      this.controlTemplates = ControlTemplates;

      this.listenTo(this.model, 'remove', this.remove);

      this.addCabControlActionsOnLoad();

      this.initializeSubscriptions();
      this.initializeSortable();
      this.initializeEventsOnCanvas();

      this.templateModel = new DesignerTplModel();
    },

    render: function () {
      return this;
    },

    initializeSubscriptions: function () {
      this.sandbox.on('initialize-sortable-on-row', this.initializeSortable, this);
      this.sandbox.on('preview-active-designer-widget', this.previewDesigner, this);
      this.sandbox.on('save-designer', this.saveWidgetDesigner, this);
    },

    addCabControlActionsOnLoad: function () {
      var options = ['<div class="cab-control-actions">',
        '<span class="label label-default">#undefined</span>',
        '<span class="label label-primary cab-move"><i class="fa fa-arrows"></i> Move</span>',
        '<span class="label label-danger deleteControlBtn"><i class="fa fa-times"></i> Remove</span>',
        '</div>'].join(''),
        formOptions = ['<div class="cab-control-actions actions-form">',
        '<span class="label label-default">#undefined</span>',
        '<span class="label label-danger form-container-settings"><i class="fa fa-gear"></i> Settings</span>',
        '</div>'].join(''),
        elementOnDesigner = this.$el.find('[data-control]:not([data-widget] [data-control], [data-control="container"]), [data-widget] > .cab-control'),
        designContainer = this.$el.find('[data-control="container"]');

      var classification = this.model.get('data').classification;
      if (classification === 'menu') {
        $(formOptions).prependTo(designContainer);
        this.bindShowFormProperty();
      }

      $(options).prependTo(elementOnDesigner);
    },

    bindShowFormProperty: function () {
      var self = this,
        formPropBtn = self.$el.find('.form-container-settings');
      formPropBtn.on('click', function (e) {
        e.preventDefault();
        self.sandbox.emit('show-form-properties');
      });
    },

    saveWidgetDesigner: function () {
      var self = this;
      if (this.$el.hasClass('active')) {
        var data = this.$el.data('widgetData'),
          content = this.$el.clone();
        content.find('.cab-control-actions').remove();
        content.find('.cab-active').removeClass('cab-active focus');
        content.find('[contenteditable]').attr('contenteditable', false);
        content.find('[cab-control="true"]').popover('destroy');
        content.find('.popover').remove();
        data.content = content.html();

        this.templateModel.urlRoot = this.sandbox.globals.HOST + 'files';
        this.templateModel.save(data).then(function () {
          self.sandbox.notifier.success(
            'Widget Designer Saved!',
            'Success!'
          );
        }, function () {
          self.sandbox.notifier.error(
            'Error saving the widget',
            'Oooops!'
          );
        });
      }
    },

    previewDesigner: function () {
      var self = this;

      if (this.$el.hasClass('active')) {
        if (this.preview.isPreview) {
          this.preview.isPreview = false;
          this.$el.html(this.preview.html);

          self.initializeSortable();
          self.initializeEventsOnCanvas();

        } else {
          this.sandbox.canvas.ACTIVE = undefined;
          this.$el.find('.cab-active').removeClass('cab-active focus');
          this.preview.isPreview = true;
          this.preview.html = this.$el.clone().html();
          this.$el.find('[data-control]').removeAttr('data-control');
          this.$el.find('.cab-control-actions').remove();
          this.$el.find('[cab-control="true"], [data-widget]').unbind();
        }
      }

    },

    initializeSortable: function (el) {
      var self = this;

      if (typeof el === 'undefined') {
        el = self.$el.find('[sortable="true"]:not([data-widget] [sortable="true"])');
      }

      console.log('sortable is called!', el);

      el.sortable({
        distance: 5,
        connectWith: '[sortable=true]',
        placeholder: 'ui-state-highlight',
        //items: '[cab-control], [data-widget]',
        handle: '.cab-move',
        sort: function (event, ui) {
          //clone control image when sorting
          var item = $(ui.item),
            itemClone = item.children().clone();
          self.$el.find('.ui-state-highlight').html(itemClone);
        },
        stop: function (event, ui) {
          var item = ui.item,
            options = ['<div class="cab-control-actions">',
            '<span class="label label-default">#undefined</span>',
            '<span class="label label-primary cab-move"><i class="fa fa-arrows"></i> Move</span>',
            '<span class="label label-danger deleteControlBtn"><i class="fa fa-times"></i> Remove</span>',
            '</div>'].join('');

          if (typeof item.attr('data-widget') === 'string') {
            if (self.model.get('data').classification === 'menu') {
              var widgetDescription = item.attr('data-widget'),
                widgetAcc = $('#cab-panel-left [data-widget="' + widgetDescription + '"]'),
                widgetData = widgetAcc.data('widget');

              var projectData = JSON.parse(self.sandbox.webStorage.sessionStorage.getItem('project-data'));
              var data = {
                projectNameSlug: projectData.slug,
                type: 'templates',
                fileName: widgetData.files.templates[0],
                classification: 'widget',
                widgetNameSlug: widgetData.slug
              };

              var widgetRow = '<div data-aura-component="' + widgetData.slug + '" data-widget=""></div>';

              self.designerModel = new DesignerModel();
              self.designerModel.urlRoot = self.sandbox.globals.HOST + 'files';
              self.designerModel.fetch({ data: data }).then(function () {
                var content = self.designerModel.get('contents'),
                  widget = $(widgetRow).html(content);
                $(options).prependTo(widget.children('.cab-control'));
                item.replaceWith(widget);
                self.initializeControlFocus(widget);
                self.initializeControlHover(widget);
                self.preventDefault();
              });
            } else {
              //add alert
              item.remove();
            }


          } else if (item.hasClass('ui-draggable')) {
            if (item.attr('data-control') === 'datasource') {
              var name = item.attr('name');
              var dataSourceControl = ['<div cab-control="true" data-control="datasource" class="form-group" data-bound-control="">',
                '<label for="inputEmail3" class="col-sm-2 control-label">' + name + '</label>',
                '<div class="col-sm-10">',
                  '<label class="pull-right" name="' + name + '">' + name + '</label>',
                '</div>',
              '</div>'].join('');
              dataSourceControl = $(dataSourceControl);
              item.replaceWith(dataSourceControl);
              $(options).prependTo(dataSourceControl);
              self.initializeEventsOnCanvas(dataSourceControl);
            } else {
              var controlType = item.attr('data-control'),
                controlTpl = $(controlTemplates[controlType]);
              //replace the drop item to real control html
              item.replaceWith(controlTpl);
              //append control options
              $(options).prependTo(controlTpl);
              //bind control events
              self.initializeEventsOnCanvas(controlTpl);
            }

          }
        }
      });
    },

    initializeEventsOnCanvas: function (el) {
      if (el) {
        //check if control has sortable
        var sortables = this.$el.find('[sortable="true"]:not([data-widget] [sortable="true"])');

        if (sortables[0]) {
          this.initializeSortable(sortables);
        }
      }

      if (!el) {
        el = this.$el.find('[cab-control=true], [data-widget]');
      }

      this.initializeControlFocus(el);
      this.initializeControlHover(el);
      this.initializeToolbar(el);
      this.preventDefault();

      var widgetsOnDesigner = this.$el.find('[data-widget]'),
        bindedSortable = widgetsOnDesigner.find('[sortable="true"]').data('uiSortable');

      if (bindedSortable) {
        widgetsOnDesigner.find('[sortable="true"]').sortable('destroy');
      }

    },

    //bind control selection event on canvas
    initializeControlFocus: function (el) {
      var self = this;

      el.on('click', function (e) {
        //to prevent other handlers from being executed
        e.stopImmediatePropagation();
        self.sandbox.emit('show-control-properties');

        var $this = $(this);
        if (!$this.hasClass('focus')) {
          self.$el.find('[cab-control="true"].focus, [data-widget].focus').removeClass('cab-active focus').unbind('keyup');
          $(document).unbind('keyup');
          $this.addClass('cab-active focus').unbind('mouseenter mouseleave');

          if (self.focusedControl) {
            self.initializeControlHover(self.focusedControl);
          }

          self.focusedControl = $this;
          self.sandbox.canvas.ACTIVE = $this;
          self.initializeDeleteHandler();
          //stored the selected control to self.focusedControl
          //add emit for properties

          if (!$(this).attr('data-aura-component')) {
            self.sandbox.emit('on-reset-property');
          }
        }

      });
    },

    //bind control hover event on canvas
    initializeControlHover: function (el) {

      el.hover(
        //add hover events on control for highlight and control options
        function (e) {
          e.stopImmediatePropagation();
          $(this).parents('[cab-control=true]').removeClass('cab-active');
          $(this).addClass('cab-active');
        },
        function (e) {
          e.stopImmediatePropagation();
          $(this).parents('[cab-control=true]:first').addClass('cab-active');
          $(this).removeClass('cab-active');
        }
      );
    },

    initializeDeleteHandler: function () {
      var self = this,
        deleteBtn = self.focusedControl.find('.deleteControlBtn');
      $(document).on('keyup', function (e) {
        if (self.focusedControl) {
          if (e.keyCode === 46) {
            //apply control focus latersss
            //if(focusBox.is(':focus')) {
            if (self.focusedControl.hasClass('cab-active focus')) {
              self.focusedControl.remove();
              $(document).unbind('keyup');
            }
            //}
          }
        }
      });

      //bind delete event on control option remove
      deleteBtn.on('click', function () {
        self.focusedControl.remove();
      });
    },

    //init toolbar for control properties
    initializeToolbar: function (el) {
      var self = this;

      //bind dblclick for editable controls
      el.on('dblclick', function (e) {
        e.stopImmediatePropagation();
        e.preventDefault();

        var element = $(this),
          control = element.find('.cab-control');

        //destroy all popover handlers
        self.$el.find('[cab-control="true"]').popover('destroy');

        //bind toolbar popover and trigger show
        element.popover({
          placement: 'bottom',
          html: 'true',
          content: function () {
            //init toolbar view
            var toolbar = new ControlToolbarView({
              sandbox: self.sandbox,
              control: control
            }).render().el;

            return toolbar;
          }
        }).popover('show');

        //adding contenteditable attribute
        control.attr('contenteditable', true);

        //highlight control editable text
        var range = document.createRange();
        range.selectNodeContents(control[0]);
        var sel = window.getSelection();
        sel.removeAllRanges();
        sel.addRange(range);

        //add focus to content editable
        control.focus();

        control.focusout(function () {
          $(this).attr('contenteditable', false);
          setTimeout(function () {
            self.$el.find('[cab-control="true"]').popover('destroy');
          }, 500);
        });

      });
    }

  });

  return AppView;

});
