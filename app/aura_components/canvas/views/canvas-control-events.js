define(['sentientview'], function (SentientView) {
  'use strict';

  var AppView = SentientView.extend({

    init: function (options) {
      this.view = options.scope;
    },

    render: function () {
      var self = this;
      return self;
    },

    bindCustomeEvents: function () {

    }

  });

  return AppView;

});
