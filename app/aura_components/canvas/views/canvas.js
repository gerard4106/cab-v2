define(['sentientview', './canvas-control-templates', './canvas-control-events', './canvas-toolbar', 'text!../templates/canvas.hbs', '../collections/canvas-designer.collection', '../views/canvas-designer', '../views/canvas-code'], function (SentientView, ControlTemplates, ControlEventsView, ControlToolbarView, template, DesignerCollection, DesignerView, CodeView) {
  'use strict';

  var AppView = SentientView.extend({

    template: template,
    isToolbarShown: false,

    init: function () {
      this.$el.html(this.template);
      new ControlEventsView({
        sandbox: this.sandbox
      }).render();
      this.designerCollection = new DesignerCollection();
      this.initializeSubscriptions();
      this.cabDesigner = this.$el.find('#cab-design');
    },

    addDesignerCollection: function (data) {
      var fileData = data.data,
        id = fileData.widgetNameSlug + '-' + fileData.type + '-' + fileData.fileName.replace(/\./g, '-');

      if (data.type === 'designer') {
        id = id + '-designer';
      } else {
        id = id + '-code';
      }

      data.id = id;

      var modelIsExisting = this.designerCollection.where({ id: id });
      if (modelIsExisting.length === 0) {
        this.designerCollection.add(data);
        this.sandbox.emit('add-designer-tab', data);
      } else {
        var panes = this.$el.find('.tab-pane');
        panes.removeClass('active');
        panes.parent().find('#' + id).addClass('active');
        this.sandbox.emit('set-active-tab', id);
      }
    },

    render: function () {
      return this;
    },

    initializeSubscriptions: function () {
      this.sandbox.on('on-show-designer', this.addDesignerCollection, this);
      this.sandbox.on('remove-designer', this.removeActiveDesigner, this);
      this.sandbox.on('on-change-designer-view', this.changeDesignerView, this);
      //this.designerCollection.on('add', this.renderDesignerView, this);
      this.listenTo(this.designerCollection, 'add', this.renderDesignerView);
    },

    changeDesignerView: function (view) {
      var cabDesignerMain = this.$el.find('#cab-design');
      cabDesignerMain.attr('data-view', view);
    },

    removeActiveDesigner: function (id) {
      id = id.replace('#', '');
      var model = this.designerCollection.where({ id: id });
      if (model.length > 0) {
        this.$el.find('.tab-pane:first').addClass('active');
        this.designerCollection.remove(model);
      }
    },

    renderDesignerView: function (model) {
      console.log('gerard: is rendering Desinger view - code style');
      var type = model.get('type');
      this.cabDesigner.children().removeClass('active');
      if (type === 'code') {
        var codeView = new CodeView({ model: model, sandbox: this.sandbox });
        this.cabDesigner.append(codeView.render().el);
      } else if (type === 'designer') {
        var designerView = new DesignerView({ model: model, sandbox: this.sandbox });
        this.cabDesigner.append(designerView.render().el);
      }
    },

    previewWidget: function () {
      var designerContent = this.$el.find('#cab-design'),
        activePane = designerContent.find('.tab-pane.active'),
        widgetClone = activePane.clone(),
        cloneId = widgetClone.attr('id') + '-preview';//cloning widget view
      activePane.removeClass('active');
      widgetClone.attr('id', cloneId);
      widgetClone.find('[data-control]').removeAttr('data-control');
      widgetClone.find('.cab-control-actions').remove();
      widgetClone.addClass('previewPointerDisable');
      designerContent.append(widgetClone);
    }

  });

  return AppView;

});
