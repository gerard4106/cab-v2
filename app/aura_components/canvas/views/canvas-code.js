define(['sentientview', '../models/ace.model'], function (SentientvView, AceModel) {
  'use strict';

  var AppView = SentientvView.extend({

    init: function () {
      var self = this;
      this.$el.attr('id', this.model.get('id'));
      this.$el.addClass('tab-pane active code-editor');
      this.aceId = this.model.get('id') + '-ace';
      this.addAceDiv();
      this.aceModel = new AceModel();

      this.listenTo(this.model, 'remove', this.remove);

      setTimeout(function () {
        self.initializeAceEditor();
      }, 200);
    },

    render: function () {
      return this;
    },

    addAceDiv: function () {
      var aceDiv = '<div id="' + this.aceId + '" ></div>';
      this.$el.html(aceDiv);
    },

    getEditorType: function () {
      var type = this.model.get('data').type;
      if (type === 'templates') {
        return 'html';
      } else if (type === 'assets') {
        return 'less';
      } else {
        return 'javascript';
      }
    },

    setEditorStyles: function () {
      //editor defined styles
      document.getElementById(this.aceId).style.fontSize = '16px';
      this.editor.getSession().setTabSize(2);
      this.editor.setHighlightActiveLine(true);
      this.editor.setShowPrintMargin(false);
      this.editor.find('needle', {
        backwards: false,
        wrap: false,
        caseSensitive: false,
        wholeWord: false,
        regExp: false
      });
      this.editor.findNext();
      this.editor.findPrevious();
    },

    initializeAceEditor: function () {
      var self = this,
        content = self.model.get('aceModel').contents;

      // file types: templates === html, assets === css, all else id js
      if ('templates' === self.model.get('data').type) {
        content = this.sandbox.formatter.formatHtml(content, {
          /*jshint camelcase: false*/
          indent_inner_html: true,
          /*jshint camelcase: false*/
          indent_size: 4
        });
      }

      self.editor = self.aceEditor.edit(self.aceId);
      self.editor.setTheme('ace/theme/monokai');
      var editorType = self.getEditorType();
      self.editor.getSession().setMode('ace/mode/' + editorType);
      self.editor.getSession().setValue(content);
      self.setEditorStyles();
      self.initializeEditorEvents();
      self.$el.find('#' + self.aceId).data('widgetData', self.model.get('data'));
    },

    initializeEditorEvents: function () {
      var self = this;

      //adding custom commands in the editor
      self.editor.commands.addCommand({
        name: 'save',
        bindKey: {
          win: 'Ctrl-S',
          mac: 'Command-S'
        },
        exec: function () {
          //add code for save here
          var fileData = $('#' + self.aceId).data('widgetData');
          fileData.content = self.editor.getValue();
          //fileData = JSON.stringify(fileData);

          self.aceModel.urlRoot = self.sandbox.globals.HOST + 'files';
          self.aceModel.save(fileData);
        },
        readOnly: true // false if this command should not apply in readOnly mode
      });

      //adding custom commands in the editor
      self.editor.commands.addCommand({
        name: 'saveAll',
        bindKey: {
          win: 'Ctrl-Shift-S',
          mac: 'Command-Shift-S'
        },
        exec: function () {
          var fileData = $('#' + self.aceId).data('widgetData');
          fileData.content = self.editor.getValue();

          self.aceModel.urlRoot = self.sandbox.globals.HOST + 'files';
          self.aceModel.save(fileData);
        },
        readOnly: true // false if this command should not apply in readOnly mode
      });
    }

  });

  return AppView;

});
