define(['sentientcollection', '../models/datasource.model'], function (SentientCollection, DataSourceModel) {
  'use strict';

  var Collection = SentientCollection.extend({
    model: DataSourceModel,

    url: function () {
      var projectId = this.sandbox.webStorage.sessionStorage.getItem('project-id');
      return this.sandbox.globals.HOST + 'datasources/' + projectId;
    },

    parse: function (response) {
      return response.data;
    }
  });

  return Collection;

});
