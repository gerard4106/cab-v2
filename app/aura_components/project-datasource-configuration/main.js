define(['./views/datasource-main.view'], function (DataSourceMainView) {
  'use strict';

  return {

    initialize: function () {
      var dataSourceMainView = new DataSourceMainView({ sandbox: this.sandbox });

      this.html(dataSourceMainView.render().el);
    }
  };

});
