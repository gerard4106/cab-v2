define(['sentientmodel'], function (SentientModel) {
  'use strict';

  var Model = SentientModel.extend({
    defaults: {
      endPoint: '',
      alias: '',
      schema: '',
      projectId: 0
    }

  });

  return Model;
});
