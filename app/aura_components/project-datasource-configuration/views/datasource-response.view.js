define(['sentientview', 'text!../templates/datasource-response.template.hbs'], function (SentientView, template) {
  'use strict';

  var AppView = SentientView.extend({

    template: template,

    init: function (options) {
      this.$el.html(this.template);
      console.log(this, options.schema);
      this.schema = options.schema;


      this.listenTo(this.model, 'sync', this.cleanup);
    },

    render: function () {
      var self = this;

      self.$el.find('#response-schema > pre').html(JSON.stringify(this.schema, undefined, 2));

      return self;
    },

    events: {
      'click #addDatasource': 'onAddDatasource'
    },

    onAddDatasource: function () {
      var schema = this.$el.find('#schema').val();
      //this.sandbox.emit('datasource-schema', schema);
      this.model.set('schema', schema);
      this.model.set('projectId', this.sandbox.webStorage.sessionStorage.getItem('project-id'));

      this.model.urlRoot = this.sandbox.globals.HOST + 'datasources';

      this.model.save();
    },

    cleanup: function () {
      this.sandbox.notifier.success(
        this.sandbox.globals.messages.generic.datasource.create.success.TITLE,
        this.sandbox.globals.messages.generic.datasource.create.success.MESSAGE
      );

      this.showWidgetOnModal('project-datasource-configuration');
      this.remove();
      this.sandbox.stop('#myModal');


    }

  });

  return AppView;
});
