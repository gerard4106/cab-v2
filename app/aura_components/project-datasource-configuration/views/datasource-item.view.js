define(['sentientview', 'text!../templates/datasource-item.template.hbs'], function (SentientView, datasourceRowTemplate) {
	'use strict';

	var AppView = SentientView.extend({

		template: datasourceRowTemplate,

		init: function () {
			var self = this;
			this.compileTemplate = self.sandbox.compileTemplate(this.template);

      this.listenTo(this.model, 'remove', function () {
        self.remove();
      }.bind(this));

      this.listenTo(this.model, 'changed', this.addDatasourceRow);

      this.initializeSubsctiptions();
		},

		initializeSubsctiptions: function () {
			this.sandbox.on('select-all-datasource', this.selectAllDatasource, this);
		},

		selectAllDatasource: function () {
			this.$el.find('.icon-check').removeClass('fa-square').addClass('fa-check-square datasource-selected');
			this.$el.find('.icon-check').attr('data-checked', true);
		},

		render: function () {
			var self = this;
			this.addDatasourceRow();
			return self;
		},

		events: {
			//'click i.icon-check': 'selectDatasource'
		},

		selectDatasource: function (e) {
			e.preventDefault();
			var checkbox = $(e.currentTarget),
				isChecked = checkbox.attr('data-checked');

			if (isChecked === 'true') {
				checkbox.attr('data-checked', 'false');
				checkbox.removeClass('datasource-selected fa-check-square');
				checkbox.addClass('fa-square');
			} else {
				checkbox.attr('data-checked', 'true');
				checkbox.addClass('fa-check-square datasource-selected');
				checkbox.addClass('datasource-selected fa-check-square');
			}
		},

		addDatasourceRow: function () {
			var model = {
				datasource: this.model.toJSON()
			};

			$(this.el).append(this.compileTemplate(model));
			this.$el.find('i.icon-check').unbind();
			this.$el.find('i.icon-check').on('click', this.selectDatasource.bind(this));
		}
	});

	return AppView;

});
