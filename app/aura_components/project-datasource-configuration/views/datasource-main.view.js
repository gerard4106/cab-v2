define([
  'sentientview',
  'text!../templates/datasource-main.template.hbs',
  './datasource.list-view',
  '../collections/datasource.collection'
], function (SentientView, template, DataSourceListView, DataSourceCollection) {
  'use strict';

  var View = SentientView.extend({
    template: template,

    init: function () {
      var dataSourceCollection = new DataSourceCollection({ sandbox: this.sandbox });
      var dataSourceListView = new DataSourceListView({ sandbox: this.sandbox, collection: dataSourceCollection });

      this.$el.html(this.template);
      this.$el.find('#datasource-list').html(dataSourceListView.render().el);
      dataSourceCollection.fetch();
    },

    render: function () {

      return this;
    }
  });

  return View;

});
