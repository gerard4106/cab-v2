define([
  'sentientview',
  'text!../templates/datasource.list-template.hbs',
  './datasource-item.view',
  './datasource-create.view',
  'text!../templates/datasource-create.template.hbs',
  '../models/datasource.model'
], function (SentientView, template, DataSourceItemView, DataSourceCreateView, datasourceCreateTemplate, DataSourceModel) {
  'use strict';

  var AppView = SentientView.extend({

    template: template,

    init: function () {
      this.$el.html(this.template);

      this.listenTo(this.collection, 'reset', this.renderDataSources);
      this.listenTo(this.collection, 'add', this.addDataSourceRow);
    },

    renderDatasources: function () {
      this.collection.forEach(this.renderDatasourceRows, this);
    },

    addDataSourceRow: function (model) {
      var datasourceRowView = new DataSourceItemView({ sandbox: this.sandbox, model: model, el: this.$el.find('#datasource-list-view') });
      datasourceRowView.render();
    },

    render: function () {
      var self = this;
      setTimeout(function () {
        $('#myModal').removeClass('flip');
      }, 180);
      return self;
    },

    events: {
      'click #addWidgetDatasource' : 'addWidgetDatasource',
      'click #datasource-select-all' : 'selectAllDatasource'
    },

    selectAllDatasource: function () {
      this.sandbox.emit('select-all-datasource');
    },

    addWidgetDatasource: function (e) {
      e.preventDefault();
      var appModal = $('#myModal');
      appModal.empty().addClass('flip');

      var dataSource = new DataSourceModel({ sandbox: this.sandbox });
      var dataSourceCreateView = new DataSourceCreateView({ sandbox: this.sandbox, model: dataSource });

      /*var componentName = 'widget-datasource';
      this.showWidgetOnModal(componentName);*/

      this.showViewOnModal(dataSourceCreateView);

      this.remove();
    }

  });

  return AppView;
});
