define([
  'sentientview',
  'text!../templates/datasource-create.template.hbs',
  './datasource-response.view'
], function (SentientView, template, DataSourceResponseView) {
  'use strict';

  var AppView = SentientView.extend({

    template: template,
    init: function () {
      this.$el.html(this.template);
      this.modelBinder.bind(this.model, this.$el);

      this.projectData = $.parseJSON(this.sandbox.webStorage.sessionStorage.getItem('project-data'));
    },

    render: function () {
      var self = this;
      setTimeout(function () {
        $('#myModal').removeClass('flip');
      }, 180);

      this.$el.find('input#host').val(this.projectData.dataSource);

      return self;
    },

    events: {
      'click #InvokeDatasource' : 'invokeDatasource',
      'click #BackToMainDatasource': 'backToMain'
    },

    backToMain: function (e) {
      e.preventDefault();
      var appModal = $('#myModal');
      appModal.empty().addClass('flip');

      var componentName = 'project-datasource';
      this.showWidgetOnModal(componentName);
    },

    invokeDatasource: function () {
      console.log(this.model.toJSON());

      var resource = this.$el.find('#endPoint').val();
      var host = this.$el.find('#host').val();

      var endpoint = host + resource;

      this.model.set({
        endPoint: endpoint,
        alias: this.$el.find('#alias').val()
      });

      var http = new this.sandbox.http({
        url: endpoint
      });

      http.getJSON().then(function (data) {

        // save schema for later parsing
        /*var schema = { response: data };
        this.sandbox.start([{
          name: 'widget-datasource-response',
          options: { el: '#myModal .modal-body', schema: schema },
        }]);*/

        var dataSourceResponseView = new DataSourceResponseView({
          el: '#myModal .modal-body',
          sandbox: this.sandbox,
          schema: { response: data },
          model: this.model
        });

        dataSourceResponseView.render();

      }.bind(this));

    }

  });

  return AppView;
});
