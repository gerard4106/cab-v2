define(['sentientview', 'text!../templates/toolbox-control-properties.hbs', 'text!../templates/toolbox-form-properties.hbs'], function (SentientView, ControlTemplate, FormTemplate) {
  'use strict';

  var AppView = SentientView.extend({

    template: ControlTemplate,

    currentProperty: undefined,

    init: function () {
      this.initializeControlProperties();
      this.initializeSubscriptions();
    },

    render: function () {
      var self = this;
      return self;
    },

    initializeSubscriptions: function () {
      this.sandbox.on('show-form-properties', this.initializeFormProperties, this);
      this.sandbox.on('show-control-properties', this.initializeControlProperties, this);
    },

    initializeControlProperties: function () {
      if (this.currentProperty !== 'control') {
        this.$el.html(ControlTemplate);
        this.currentProperty = 'control';
        this.sandbox.start([{
          name: 'toolbox-properties-structure',
          options : { el: 'div#element-properties' }
        }, {
          name: 'toolbox-properties-effects',
          options : { el: 'div#effects-properties' }
        }, {
          name: 'toolbox-properties-box',
          options : { el: 'div#box-properties' }
        }, {
          name: 'toolbox-properties-bootstrap',
          options : { el: 'div#bootstrap-properties' }
        }, {
          name: 'toolbox-control-properties',
          options: { el: 'div#control-properties' }
        }]);
      }
    },

    initializeFormProperties: function () {
      if (this.currentProperty !== 'form') {
        this.$el.html(FormTemplate);
        this.currentProperty = 'form';
        this.sandbox.start([{
          name: 'toolbox-properties-form',
          options : { el: 'div#form-properties' }
        }]);
      }
    }

  });

  return AppView;

});
