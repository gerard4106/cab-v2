define(['backbone'], function (Backbone) {
  'use strict';

  var ProjectCollection = Backbone.Collection.extend({
    defaults: {}
  });

  return ProjectCollection;

});