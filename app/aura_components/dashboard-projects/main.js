define(['./views/dashboard-projects'], function (ProjectsListView) {
  'use strict';

  return {

    initialize: function () {
      var projectList = new ProjectsListView({ sandbox: this.sandbox });
      this.html(projectList.render().el);
    }

  };

});
