define(['sentientview', 'text!../templates/dashboard-projects.hbs', '../collections/projects.collection'], function (SentientView, template, ProjectsCollection) {
  'use strict';

  var AppView = SentientView.extend({

    template: template,

    init: function () {
      var self = this;
      self.projectCollection = new ProjectsCollection();
      self.compiledTemplate = self.sandbox.compileTemplate(self.template);
      self.getProjects();
    },

    getProjects: function () {
      var self = this;
      self.projectCollection.url = this.sandbox.globals.HOST + 'projects';
      self.projectCollection.fetch().then(function () {
        var projects = self.projectCollection.toJSON();
        self.$el.html(self.compiledTemplate(projects));
        //self.sandbox.emit('on-remove-loading');
      });
    },

    events: {

    },

    render: function () {
      return this;
    }

  });

  return AppView;

});
