define(['sentientview', 'text!../templates/toolbox-controls-datasource.hbs', 'text!../templates/toolbox-controls-list.hbs'], function (SentientView, template, datasourceListTemplate) {
  'use strict';

  var AppView = SentientView.extend({

    template: template,
    init: function () {
      this.$el.html(this.template);
      this.initializeSubscriptions();
    },

    render: function () {
      var self = this;
      return self;
    },

    initializeSubscriptions: function () {
      this.sandbox.on('show-datasource-draggables', this.buildDatasourceToolbox, this);
    },

    buildDatasourceToolbox: function (data) {
      var self = this;
      var datasource = data.dataSources[0];
      var datasourceUrl = datasource.endPoint;
      var compileTemplate = this.sandbox.compileTemplate(datasourceListTemplate);
      var schema = datasource.schema.split('.');

      this.model.urlRoot = datasourceUrl;
      this.model.fetch().then(function () {
        var data = self.model.toJSON();
        var arr = [];

        $(schema).each(function (i, val) {
          if (val !== 'response') {
            data = data[val];
          }
        });

        for (var key in data) {
          if (data.hasOwnProperty(key)) {
            arr.push(key);
          }
        }

        var attributes = {
          attributes : arr
        };

        self.$el.find('#datasource-draggable-list').html(compileTemplate(attributes));

        self.initializeToolboxDraggable();

      });
    },

    initializeToolboxDraggable: function () {
      this.$el.find('[draggable=true]').draggable({
        cursor: 'move',
        cursorAt: { top: -5, left: -5 },
        helper: 'clone',
        opacity: 0.7,
        connectToSortable: '[sortable=true]',
        zIndex: 9999
      });
    },

  });

  return AppView;

});
