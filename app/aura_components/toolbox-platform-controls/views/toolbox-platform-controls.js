define(['sentientview', 'text!../templates/toolbox-platform-controls.hbs'], function (SentientView, template) {
  'use strict';

  var AppView = SentientView.extend({

    template: template,

    init: function () {
      var self = this;
      self.$el.html(this.template);
      self.bindDocumentSave();
    },

    render: function () {
      var self = this;
      return self;
    },

    events: {
      'click #save-file' : 'onSaveActiveFile',
      'click #active-widget-preview' : 'onPreviewWidget',
      'click a#launchProject': 'onLaunchProject'
    },

    bindDocumentSave: function () {
      var self = this;
      $(document).bind('keydown', function (e) {
        if (e.ctrlKey && (e.which === 83)) {
          self.onSaveActiveFile();
          e.preventDefault();
        }
      });
    },

    onSaveActiveFile: function () {
      this.sandbox.emit('save-designer');
    },

    onPreviewWidget: function () {
      this.sandbox.emit('preview-active-designer-widget');
    },

    onLaunchProject: function (e) {
      e.preventDefault();

      var projectNameSlug = this.sandbox.webStorage.sessionStorage.getItem('projectNameSlug');
      var url = this.sandbox.globals.SENTIENT_WEB_SERVER + projectNameSlug + '/app';
      window.open(url);
    }

  });

  return AppView;

});
