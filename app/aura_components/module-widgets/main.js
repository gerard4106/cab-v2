define(['./views/widgets.list-view', './collections/widgets.collection'], function (AppView, WidgetCollection) {
  'use strict';

  return {

    initialize: function () {

      var widgets = new WidgetCollection({ sandbox: this.sandbox });
      var app = new AppView({ sandbox: this.sandbox, collection: widgets });

      //widgets.fetch();
      this.html(app.render().el);

    }

  };

});
