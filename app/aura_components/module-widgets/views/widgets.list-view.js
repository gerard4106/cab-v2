define(['sentientview', 'text!../templates/widgets.hbs', '../collections/widgets.collection', '../collections/datasource.collection', './widget-view'], function (SentientView, template, WidgetCollection, DatasourceCollection, WidgetView) {
  'use strict';

  var AppView = SentientView.extend({

    template: template,

    init: function () {
      //this.listenTo(this.collection, 'reset', this.renderWidgets);
      this.listenTo(this.collection, 'add', this.addOneWidget);
      this.listenTo(this.collection, 'reset', this.renderWidgets);
      this.sandbox.on('widget-created', this.addWidgetToCollection, this);
      this.collection.fetch();
    },

    render: function () {
      this.$el.html(this.template);
      this.$widgetItems = this.$el.find('#widget-items');
      /*this.collection.fetch().then(function () {
        this.renderWidgets();
      }.bind(this));*/
      return this;
    },

    renderWidgets: function () {
      console.log('on render widgets', this.collection.toJSON());
      this.collection.forEach(this.addOneWidget, this);
    },

    addOneWidget: function (model) {
      model.set(model.get('widgetAttributes'));
      //model.set('files', clone.files);
      model.unset('widgetAttributes');
      var widgetView = new WidgetView({ sandbox: this.sandbox, model: model });
      this.$widgetItems.append(widgetView.render().el);
    },

    addWidgetToCollection: function (model) {
      this.collection.add(model);
    }

  });

  return AppView;

});
