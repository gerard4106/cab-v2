define([
  'sentientview',
  'text!../templates/widget.hbs',
  '../models/widget.model',
  './widget-update.view'
], function (SentientView, template, WidgetModel, EditWidgetView) {
  'use strict';

  var AppView = SentientView.extend({

    template: template,
    init: function () {
      var self = this;
      var compiledTemplate = self.sandbox.compileTemplate(self.template);

      console.log('the widget model on bind', this.model.toJSON());
      //this.model.set('dataSources', self.dataSources);
      this.$el.html(compiledTemplate(this.model.toJSON()));
    },

    initializeWidgetDraggable: function () {
      //bind data on widget draggable
      this.$el.find('[data-tree-header]').data('widget', this.model.toJSON());

      this.$el.find('[data-tree-header]').draggable({
        cursor: 'move',
        cursorAt: { top: -5, left: -5 },
        helper: function () {
          var label = '<label>' + $(this).data('widget').name + '</label>';
          return  label;
        },
        opacity: 0.8,
        connectToSortable: '[sortable=true]',
        zIndex: 9999
      });
    },

    initializeContextMenu: function () {
      var self = this;
      $.contextMenu({
        selector: '[data-tree-header="widget"]',
        className: 'dropdown-menu',
        callback: function (key) { // key, options
          var fileData = $(this).data('widget');
          if (key === 'view') {
            var projectData = JSON.parse(self.sandbox.webStorage.sessionStorage.getItem('project-data'));
            var data = {
              projectNameSlug: projectData.slug,
              type: self.sandbox.globals.fileTypes.TEMPLATES,
              fileName: fileData.files.templates[0],
              classification: self.sandbox.globals.fileClassifications.WIDGET,
              widgetNameSlug: fileData.slug
            };

            self.designerModel = new WidgetModel();
            self.designerModel.urlRoot = self.sandbox.globals.HOST + 'files';
            self.designerModel.fetch({ data: data }).then(function () {
              var widgetData = {
                data: data,
                designerModel: self.designerModel.toJSON(),
                type: 'designer'
              };
              self.sandbox.emit('on-show-designer', widgetData);
            });

            self.sandbox.emit('show-datasource-draggables', fileData);
          } else if (key === 'edit') {
            self.editWidget();
          }
        },
        items: {
          'move': {name: 'Move', icon: 'edit'},
          'edit': { name: 'Edit', icon: 'edit' },
          'rename': {name: 'Rename', icon: 'cut'},
          'view': {name: 'View Designer', icon: 'copy'},
          'sep1': '---------',
          'delete': {name: 'Delete', icon: 'quit'}
        }
      });
    },

		toggle: function () {
			$('.cab-fa-ul a').off('click').click(function (e) {
				e.preventDefault();
        var folder = $(this).siblings('ul');
				folder.slideToggle().find('a');
			});
		},

    render: function () {
      var self = this;

      setTimeout(function () {
        self.toggle();
      }, 500);

      self.initializeContextMenu();
      self.initializeWidgetDraggable();
      return this;
    },

    events: {
      'dblclick .widget-files': 'aceShowCode'
    },

    aceShowCode: function (e) {
      var self = this;
      var projectData = JSON.parse(self.sandbox.webStorage.sessionStorage.getItem('project-data'));
      var target = $(e.target),
        parent = target.parents('.collapse.parent-tree'),
        data = {
          projectNameSlug: projectData.slug,
          type: target.attr('data-type'),
          fileName: target.attr('data-filename'),
          classification: parent.attr('data-classification'),
          widgetNameSlug: parent.attr('id').replace('-widget', '')
        };
      self.aceModel = new WidgetModel();
      self.aceModel.urlRoot = self.sandbox.globals.HOST + 'files';
      self.aceModel.fetch({ data: data }).then(function () {
        var widgetData = {
          data: data,
          aceModel: self.aceModel.toJSON(),
          type: 'code'
        };
        self.sandbox.emit('on-show-designer', widgetData);
      });
    },

    editWidget: function () {

      // selected properties only
      /*var _widget = _.pick(this.widgetAttributes, 'widgetAttributes', 'dataSources');
      var widget = new WidgetModel(_widget.widgetAttributes);
      widget.sandbox = this.sandbox;
      //widget.set('sandbox', this.sandbox);
      widget.set('dataSources', _widget.dataSources);*/

      var editWidget = new EditWidgetView({ sandbox: this.sandbox, model: this.model });
      this.showViewOnModal(editWidget);
    }

  });

  return AppView;
});
