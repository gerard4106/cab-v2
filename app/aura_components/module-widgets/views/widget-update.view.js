define(['sentientview', 'text!../templates/widget-update.hbs'],
       function (SentientView, template) {
  'use strict';

  var View = SentientView.extend({

    template: template,

    init: function () {
      this.template = this.sandbox.compileTemplate(this.template);
      this.listenTo(this.model, 'sync', this.modelSynced);
    },

    render: function () {

      this.$el.html(this.template(this.model.toJSON()));
      this.modelBinder.bind(this.model, this.$el);
      return this;
    },

    events: {
      'click #update-widget': 'onUpdateWidget'
    },

    onUpdateWidget: function () {
      this.model.save();
    },

    modelSynced: function () {
      this.sandbox.notifier.success(
        this.sandbox.globals.messages.generic.UPDATE_WIDGET.success.title,
        this.sandbox.globals.messages.generic.UPDATE_WIDGET.success.message
      );
    }
  });

  return View;

});
