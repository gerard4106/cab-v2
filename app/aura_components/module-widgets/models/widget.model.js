define(['sentientmodel'], function (SentientModel) {
  'use strict';

  var WidgetModel = SentientModel.extend({
    urlRoot: function () {
      console.log(this);
      return this.sandbox.globals.HOST + 'widgets';
    },
    defaults: {},
    parse: function (response) {
      var widget = response.data;
      return widget;
    }
  });

  return WidgetModel;

});
