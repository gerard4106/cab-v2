define(['backbone'], function (Backbone) {
  'use strict';

  var DatasourceCollection = Backbone.Collection.extend({
    defaults: {},
    parse: function (response) {
      return response.data;
    }
  });

  return DatasourceCollection;

});