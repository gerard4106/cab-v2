define(['sentientcollection'], function (SentientCollection) {
  'use strict';

  var WidgetCollection = SentientCollection.extend({
    url: function () {
      var projectId = this.sandbox.webStorage.sessionStorage.getItem('project-id');
      var projectNameSlug = this.sandbox.webStorage.sessionStorage.getItem('projectNameSlug');
      return this.sandbox.globals.HOST + 'widgets/' + projectId + '/' + projectNameSlug;
    },
    parse: function (response) {
      return response.data.widgets;
    }
  });

  return WidgetCollection;

});
