define([
  '../views/login',
  '../views/dashboard',
  '../views/appbuilder'
], function (LoginView, DashboardView, AppBuilderView) {
  'use strict';

  var helper = {
    LoginView: LoginView,
    DashboardView: DashboardView,
    AppBuilderView: AppBuilderView
  };

  return helper;

});
