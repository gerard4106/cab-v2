define(['backbone'], function (Backbone) {
  'use strict';

  var Router = Backbone.Router.extend({

    initialize: function (options) {
      this.sandbox = options.sandbox;
      this.tracker = options.tracker();
      this.views = options.views;
      Backbone.history.start();
    },

    routes: {
      'dashboard': 'dashboardRoute',
      'appbuilder/:projectId': 'appbuilderRoute',
      '': 'login'
    },

    beforePageRefresh: function () {
      var self = this;
      function removeSession() {
        console.log('remove');
        self.sandbox.webStorage.sessionStorage.removeItem('isHeaderLoaded');
      }
      window.onbeforeunload = removeSession();
    },

    login: function () {

      this.sandbox.emit('undelegate');
      //this.sandbox.emit('on-add-loading');

      var login = new this.views.LoginView({ sandbox: this.sandbox });

      $('div#cab-container').html(login.render().el);
      this.tracker.currentView = login;
    },

    dashboardRoute: function () {

      this.sandbox.emit('undelegate');
      //this.sandbox.emit('on-add-loading');

      var dashboard = new this.views.DashboardView({ sandbox: this.sandbox });

      $('div#cab-container').html(dashboard.render().el);
      this.tracker.currentView = dashboard;

    },

    appbuilderRoute: function (projectId) {

      this.sandbox.emit('undelegate');
      //this.sandbox.emit('on-add-loading');

      var appbuilder = new this.views.AppBuilderView({ sandbox: this.sandbox, projectId: projectId });

      $('div#cab-container').html(appbuilder.render().el);
      this.tracker.currentView = appbuilder;
    },

    nofound: function () {
      console.log('not found');
    }
  });

  return Router;

});


