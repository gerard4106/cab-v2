define([
  './router',
  './helpers/menu.helper',
  'bootstrap',
  'jqueryuiDraggable',
  'jqueryuiSortable',
  'colorpicker',
  'contextmenu'
], function (Router, MenuHelper) {
  'use strict';

  return {

    initialize: function () {
      this.initializeRouter();
    },

    initializeRouter: function () {
      // instantiate router, pass sandbox and helper methods
      new Router({
        sandbox: this.sandbox,
        tracker: this.tracker,
        views: MenuHelper
      });
    },

    tracker: function () {
      return {
        currentView: '',
        switchView: function () {
          // pending
        }
      };
    },
  };

});
