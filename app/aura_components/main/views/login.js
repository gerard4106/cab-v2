define(['sentientview', 'text!../templates/login.hbs'], function (SentientView, template) {
  'use strict';

  var LoginView = SentientView.extend({

    template: template,

    init: function () {
      this.$el.html(this.template);
			this.bodyClass();
    },

    render: function () {

      return this;
    },

		bodyClass: function () {
			$('body').addClass('cab-login');
		}

  });

  return LoginView;

});
