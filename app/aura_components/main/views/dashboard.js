define(['sentientview', 'text!../templates/dashboard.hbs'], function (SentientView, template) {
  'use strict';

  var DashboardView = SentientView.extend({

    template: template,

    init: function (options) {
      this.sandbox = options.sandbox;

      this.$el.html(this.template);

			this.bodyClass();
    },

    render: function () {
      this.checkExistingHeader();

      this.sandbox.start([{
        name: 'dashboard-projects',
        options: { el: '#projcontent' }
      }]);

      return this;
    },

		bodyClass: function () {
			$('body').removeClass('cab-login');
			$('body').addClass('cab-dashboard');
		},

    events: {
      'click #new-project' : 'onNewProject'
    },

    onNewProject: function (e) {
      e.preventDefault();
      var componentName = 'dashboard-new-project';
      this.showWidgetOnModal(componentName);
    }

  });

  return DashboardView;

});
