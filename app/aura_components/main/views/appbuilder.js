define(['sentientview', 'text!../templates/appbuilder.hbs', '../models/appbuilder.model'], function (SentientView, template, ProjectModel) {
  'use strict';

  var AppBuilderView = SentientView.extend({

    template: template,

    init: function (options) {
      var self = this;
      self.$el.html(this.template);
      this.projectId = options.projectId;
      //self.initializeBuilderMainView();
      self.getProjectData();
    },

    render: function () {
      return this;
    },

    getProjectData: function () {
      var self = this;
      var projectModel = new ProjectModel();
      projectModel.urlRoot = this.sandbox.globals.HOST + 'projects/' + this.projectId;
      projectModel.fetch().then(function () {
        var projectData = projectModel.toJSON();

        self.sandbox.webStorage.sessionStorage.removeItem('project-data');
        self.sandbox.webStorage.sessionStorage.removeItem('projectNameSlug');

        self.sandbox.webStorage.sessionStorage.setItem('project-data', JSON.stringify(projectData));
        self.sandbox.webStorage.sessionStorage.setItem('projectNameSlug', projectData.slug);
        self.sandbox.webStorage.sessionStorage.setItem('project-id', projectData.id);

        var projecttype = self.sandbox.globals.projectType[projectModel.get('ProjectTypeId')];
        self.sandbox.webStorage.sessionStorage.setItem('project-type', projecttype);

        self.initializeBuilderAppView();
        self.aPreventDefault();
        self.panelEvents();
        self.bodyClass();

        setTimeout(function () {
          self.sandbox.emit('on-load-project');
        }, 500);
      });
    },

    initializeBuilderAppView: function () {
      var self = this;
      self.checkExistingHeader();

      self.sandbox.start([{
        name: 'canvas',
        options : { el: 'div#cab-panel' }
      }, {
        name: 'module',
        options : { el: 'div#cab-panel-left' }
      }, {
        name: 'toolbox',
        options : { el: 'div#cab-panel-right' }
      }, {
        name: 'footer',
        options: { el: '#cab-footer' }
      }]);
    },

    initializeBuilderWidgetView: function () {
      var self = this;
      self.sandbox.start([{
        name: 'canvas',
        options : { el: 'div#cab-panel' }
      }, {
        name: 'module',
        options : { el: 'div#cab-panel-left' }
      }, {
        name: 'toolbox',
        options : { el: 'div#cab-panel-right' }
      }, {
        name: 'header',
        options: { el: '#cab-header' }
      }, {
        name: 'footer',
        options: { el: '#cab-footer' }
      }]);
    },

    aPreventDefault: function () {
      var self = this;
      self.$el.find('a').on('click', function (e) {
        e.preventDefault();
      });
    },

    panelEvents: function () {
      var self = this;

      //changing icon state
      self.$el.find('.cab-toggle-panel a').click(function (e) {
        e.preventDefault();
        $(this).children('i').toggleClass('fa-angle-left fa-angle-right');
      });

      //toggle hide left panel
      self.$el.find('.cab-toggle-panel.left a').click(function (e) {
        e.preventDefault();
        $('#cab-container').toggleClass('hide_left');
      });

      //toggle hide right panel
      self.$el.find('.cab-toggle-panel.right a').click(function (e) {
        e.preventDefault();
        $('#cab-container').toggleClass('hide_right');
      });
    },

		bodyClass: function () {
			$('body').removeClass('cab-dashboard');
		}

  });

  return AppBuilderView;

});
