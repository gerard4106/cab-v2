define(['backbone'], function (Backbone) {
  'use strict';

  var ProjectModel = Backbone.Model.extend({
    defaults: {},
    parse: function (reponse) {
      var data = reponse.data;
      return data;
    }
  });

  return ProjectModel;

});