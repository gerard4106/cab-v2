define(['./views/toolbox-icon-library'], function (AppView) {
  'use strict';

  return {

    initialize: function () {
      var app = new AppView({ sandbox: this.sandbox });
      this.html(app.render().el);
    }

  };

});