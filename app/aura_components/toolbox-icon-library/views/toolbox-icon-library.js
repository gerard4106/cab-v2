define(['sentientview', 'text!../templates/toolbox-icon-library.hbs'], function (SentientView, template) {
  'use strict';

  var AppView = SentientView.extend({

    template: template,

    init: function () {
      this.$el.html(this.template);
    },

    render: function () {
      return this;
    },

    events: {
      'click #icon-library-content a.btn-block' : 'onSelectIcon'
    },

    onSelectIcon: function (e) {
      e.preventDefault();
      //var icons = this.$el.find('#icon-library-content a.btn-block').removeClass('btn-primary');
      $(e.target).addClass('btn-primary');
    }

  });

  return AppView;

});