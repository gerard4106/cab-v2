define(['sentientview', 'text!../templates/toolbox-events.hbs'], function (SentientView, template) {
  'use strict';

  var AppView = SentientView.extend({

    template: template,
    initialize: function (options) {
      var self = this;
      self.sandbox = options.sandbox;
      self.$el.html(this.template);
    },

    render: function () {
      var self = this;

      return self;
    }

  });

  return AppView;

});