define(['./views/dashboard-new-project', './models/new-project.model'], function (AppView, ProjectModel) {
  'use strict';

  return {

    initialize: function () {

      var projectModel = new ProjectModel({ sandbox: this.sandbox });

      var app = new AppView({ sandbox: this.sandbox, model: projectModel });
      this.html(app.render().el);
    }

  };

});
