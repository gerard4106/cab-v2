define([
  'sentientview',
  'text!../templates/dashboard-new-project.hbs',
  '../policies/dashboard-new-project-policies'
], function (SentientView, template, Policies) {
  'use strict';

  var AppView = SentientView.extend({

    template: template,
    projectTypeId: 4,
    init: function () {
      this.$el.html(this.template);

      // set default project type
      // TODO: transefer in model defaults
      //this.model.set('projectTypeId', 4);
      this.model.set('themeType', 1);
      this.model.set('themeId', 1);
    },

    render: function () {
      var self = this;
      setTimeout(function () {
        self.$el.find('#NewProjectName').focus();
      }, 500);
			this.setActiveThumbnail();
      return self;
    },

    events: {
      'click #CreateNewProject' : 'createNewProject',
      'keyup #NewProjectName' : 'getEnterEvent',
      'click .project-type': 'setProjectType',
      'click .project-theme': 'selectTheme',
      'click .custom-theme': 'selectCustomTheme'
    },

    selectCustomTheme: function (e) {
      e.preventDefault();
    },

		setActiveThumbnail: function () {
			var self = this;
			self.$el.find('.thumbnail a').click(function () {
				$(this).closest('.row').find('.thumbnail').removeClass('active');
				$(this).parent('.thumbnail').addClass('active');
			});
		},

    getEnterEvent: function (e) {
      if (e.keyCode === 13) {
        this.createNewProject();
      }
    },

    setProjectType: function (e) {
      this.model.set('projectTypeId', $(e.currentTarget).data().projectTypeId);
    },

    createNewProject: function () {
      var self = this,
        createBtn = self.$el.find('#CreateNewProject');

      self.sandbox.processNotifier.notify(createBtn);

      var projectTypeId = self.$el.find('.active > .project-type').data().projectTypeId;
      self.model.set('projectTypeId', projectTypeId);

      Policies.mustBeAValidProjectType({ model: self.model, sandbox: self.sandbox, el: self.$el }, function (res) {
        if (res) {

          self.model.save().then(function (data) {
            data = data.data.project;
            $('#myModal').modal('hide');
            self.sandbox.processNotifier.stop(createBtn);
            window.location.hash = '#appbuilder/' + data.id;
          });

        } else {
          // TODO: show standard message component passing in sandbox.globals.message.approporiatemeassage
          createBtn.html('Create');
          console.log('not valid');
        }
      }.bind(self));
    },

    selectTheme: function (e) {
      e.preventDefault();
      var targetData = $(e.currentTarget).data();

      this.model.set('themeId', targetData.themeId);
      this.model.set('themeType', targetData.themeType);
    }

  });

  return AppView;

});
