define(['underscore'], function (_) {

  'use strict';

  var Policies = {

    mustBeAValidProjectType: function (options, cb) {
      var validTypes = [1, 2, 3, 4, 5];
      var projectTypeId = options.model.get('projectTypeId');
      var el = options.el;

      if (_.indexOf(validTypes, projectTypeId) === -1) {

        cb(false);

      } else {

        var projectType = options.sandbox.globals.projectType[projectTypeId];

        switch (projectType) {

        case 'widget':
          options.model.set('name', el.find('#widget-name').val());
          options.model.set('instance', el.find('#widget-instance').val());
          options.model.urlRoot = options.sandbox.globals.HOST + 'widgets';
          cb(true);
          break;

        case 'app':
          options.model.set('name', el.find('#NewProjectName').val());
          options.model.set('dataSource', el.find('#dataSource').val());
          options.model.urlRoot = options.sandbox.globals.HOST + 'projects';
          cb(true);
          break;

        case 'control':
          console.log('control');
          cb(true);
          break;

        case 'theme':
          console.log('theme');
          cb(true);
          break;

        case 'website':
          console.log('website');
          cb(true);
          break;

        }

      }
    }
  };


  return Policies;

});
