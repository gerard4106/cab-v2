define(['sentientmodel'], function (SentientModel) {
  'use strict';

  var ProjectModel = SentientModel.extend({
    defaults: {}
  });

  return ProjectModel;

});
