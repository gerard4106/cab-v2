define(['./views/canvas-ace'], function (AceView) {
  'use strict';

  return {

    initialize: function () {
      var app = new AceView({ sandbox: this.sandbox, widgetAceData: this.options.params });
      this.html(app.render().el);
    }

  };

});