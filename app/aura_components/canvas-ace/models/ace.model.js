define(['backbone'], function (Backbone) {
  'use strict';

  var AceModel = Backbone.Model.extend({
    defaults: {}
  });

  return AceModel;

});