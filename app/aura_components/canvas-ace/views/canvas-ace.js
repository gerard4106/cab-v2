define(['sentientview', 'ace', '../models/ace.model', 'text!../templates/canvas-ace.hbs'], function (SentientView, ace, AceModel, template) {
  'use strict';

  var AppView = SentientView.extend({

    template: template,

    init: function (options) {
      var self = this;
      this.$el.html(this.template);
      this.aceModel = new AceModel();
      this.widgetAceData = options.widgetAceData;
      this.aceId = this.widgetAceData.id + '-ace';
      this.$el.find('.ace-editor').attr('id', this.aceId);

      setTimeout(function () {
        self.initializeAceEditor();
      }, 200);
    },

    render: function () {
      var self = this;
      return self;
    },

    //handle script type here
    getEditorType: function () {
      var type = this.widgetAceData;
      if (type.data.type === 'templates') {
        return 'html';
      } else {
        return 'javascript';
      }
    },

    initializeAceEditor: function () {
      var self = this,
        content = self.widgetAceData.aceModel.get('contents');

      self.editor = ace.edit(this.aceId);
      self.editor.setTheme('ace/theme/monokai');
      var editorType = self.getEditorType();
      self.editor.getSession().setMode('ace/mode/' + editorType);

      // beautify

      self.editor.getSession().setValue(content);
      self.setEditorStyles();
      self.initializeEditorEvents();
      self.$el.find('#' + this.aceId).data('widgetData', self.widgetAceData.data);
    },

    setEditorStyles: function () {
      //editor defined styles
      document.getElementById(this.aceId).style.fontSize = '16px';
      this.editor.getSession().setTabSize(2);
      this.editor.setHighlightActiveLine(true);
      this.editor.setShowPrintMargin(false);
      this.editor.find('needle', {
        backwards: false,
        wrap: false,
        caseSensitive: false,
        wholeWord: false,
        regExp: false
      });
      this.editor.findNext();
      this.editor.findPrevious();
    },

    initializeEditorEvents: function () {
      var self = this;

      //adding custom commands in the editor
      self.editor.commands.addCommand({
        name: 'save',
        bindKey: {
          win: 'Ctrl-S',
          mac: 'Command-S'
        },
        exec: function () {
          //add code for save here
          var fileData = $('#' + self.aceId).data('widgetData');
          fileData.content = self.editor.getValue();
          //fileData = JSON.stringify(fileData);
          self.aceModel.urlRoot = self.sandbox.globals.HOST + 'files';
          self.aceModel.save(fileData).then(function () {
            console.log('file saved');
            self.sandbox.notifier.success(
              'Success!',
              'File has been saved!'
            );
          });
        },
        readOnly: true // false if this command should not apply in readOnly mode
      });

      //adding custom commands in the editor
      self.editor.commands.addCommand({
        name: 'saveAll',
        bindKey: {
          win: 'Ctrl-Shift-S',
          mac: 'Command-Shift-S'
        },
        exec: function () {
          var fileData = $('#' + self.aceId).data('widgetData');
          fileData.content = self.editor.getValue();

          self.aceModel.urlRoot = self.sandbox.globals.HOST + 'files/';
          self.aceModel.save(fileData);
        },
        readOnly: true // false if this command should not apply in readOnly mode
      });
    }

  });

  return AppView;

});
