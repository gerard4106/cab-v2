define(['sentientview', 'text!../templates/toolbox-properties-structure.hbs'], function (SentientView, template) {
  'use strict';

  var AppView = SentientView.extend({

    template: template,

    init: function () {
      this.html(this.template);
      this.getControlProperties();
      this.initializeSubscriptions();
    },

    render: function () {
      return this;
    },

    events: {
      'blur #structure-id' : 'structureId',
      'blur #structure-class' : 'structureClass',
      'change #structure-block' : 'structureBlock',
      'blur #structure-icon-class' : 'structureIconClass',
      'click #structure-icon-browse' : 'structureIconLibrary'
    },

    initializeSubscriptions: function () {
      this.sandbox.on('on-reset-property', this.getControlProperties, this);
    },

    getControlProperties: function () {
      if (this.checkActiveControl()) {
        this.$el.find('#structure-id').val(this.focusedControl.attr('id'));
        this.$el.find('#structure-class').val(this.focusedControl.attr('class'));
        var isBlockElement = this.focusedControl.parent().css('display') === 'block' ? true : false;
        if (isBlockElement) {
          this.$el.find('#structure-block').prop('checked', true);
        } else {
          this.$el.find('#structure-block').prop('checked', false);
        }
        var icon = this.focusedControl.find('i');
        if (icon[0]) {
          this.$el.find('#structure-icon-class').val(icon.attr('class'));
        }
      }
    },

    structureIconLibrary: function () {
      //if (this.checkActiveControl()) {
      this.showWidgetOnModal('toolbox-icon-library');
      //}
    },

    structureId: function (e) {
      if (this.checkActiveControl()) {
        var val = $(e.target).val();
        this.focusedControl.attr('id', val);
      }
    },

    structureClass: function (e) {
      if (this.checkActiveControl()) {
        var val = $(e.target).val();
        this.focusedControl.attr('class', val);
      }
    },

    structureBlock: function () {
      if (this.checkActiveControl()) {
        if (this.checked) {
          this.focusedControl.parent().css('display', 'block');
        } else {
          this.focusedControl.parent().css('display', 'inline-block');
        }
      }
    },

    structureIconClass: function (e) {
      if (this.checkActiveControl()) {
        var icon = this.focusedControl.find('i');
        if (icon[0]) {
          var val = $(e.target).val();
          icon.attr('class', val);
        }
      }
    }

  });

  return AppView;

});