define(['./views/app'], function (AppView) {
  'use strict';

  return {

    initialize: function () {
      var app = new AppView({ sandbox: this.sandbox });
      $('#cab-modal').html(app.render().el);
    }

  };

});

