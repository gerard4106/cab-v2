define(['sentientview', 'text!../templates/modal.hbs'], function (SentientView, template) {
	'use strict';

	var AppView = SentientView.extend({

		template: template,
    isGlobalWidget: true,

		init: function () {
			this.$el.html(this.template);
		},

		render: function () {
			var self = this;

			self.$el.on('hidden.bs.modal', function () {
				self.$el.find('#myModal').empty();
			});

			return self;
		}

	});

	return AppView;
});
