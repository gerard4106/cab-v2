define(['sentientview', 'text!../templates/toolbox.hbs'], function (SentientView, template) {
  'use strict';

  var AppView = SentientView.extend({

    template: template,
    isPropertiesLoaded: false,
    isEventsLoaded: false,
    initialize: function (options) {
      var self = this;
      self.sandbox = options.sandbox;
      self.selectors = self.sandbox.globals.selectors;
      self.$el.html(this.template);
      self.initializeWidgets();
    },

    render: function () {
      return this;
    },

    events: {
      'click #properties-view' : 'initPropertiesView',
      'click #events-view' : 'initEventsView'
    },

    initEventsView: _.once(function () {
      this.sandbox.start([{
        name : 'toolbox-events',
        options: { el: this.selectors.rightPanel.EVENTS }
      }]);
    }),

    initPropertiesView: function () {
      if (!this.isPropertiesLoaded) {
        this.sandbox.start([{
          name : 'toolbox-properties',
          options: { el: this.selectors.rightPanel.PROPERTIES }
        }]);
        this.isPropertiesLoaded = true;
      }
    },

    initializeWidgets: function () {
      if (!this.isEventsLoaded) {
        this.sandbox.start([{
          name : 'toolbox-controls',
          options: { el: this.selectors.rightPanel.CONTROLS }
        }, {
          name : 'toolbox-platform-controls',
          options: { el: this.selectors.rightPanel.HEADING }
        }]);
        this.isEventsLoaded = true;
      }
    }

  });

  return AppView;

});
