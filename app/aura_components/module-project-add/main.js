define(['./views/menu.view', './models/menu.model'], function (MenuView, MenuModel) {
  'use strict';

  return {

    initialize: function () {

      var session = this.sandbox.webStorage.sessionStorage;

      var menu = new MenuModel({ sandbox: this.sandbox, projectId: session.getItem('project-id'), projectNameSlug: session.getItem('projectNameSlug') });
      var menuView = new MenuView({ sandbox: this.sandbox, model: menu });

      this.html(menuView.render().el);
    }
  };

});
