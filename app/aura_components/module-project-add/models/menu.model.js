define(['sentientmodel'], function (SentientModel) {
  'use strict';

  var Model = SentientModel.extend({
    urlRoot: function () {
      return this.sandbox.globals.HOST + 'menus';
    },
    defaults: {
      name: '',
      parentId: null,
      description: ''
    },

    parse: function (response) {
      return response.data.createdMenu;
    }
  });

  return Model;

});
