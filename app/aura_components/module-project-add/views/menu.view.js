define(['sentientview', 'text!../templates/menu.template.hbs'], function (SentientView, template) {
  'use strict';

  var View = SentientView.extend({
    template: template,

    init: function () {

    },

    render: function () {
      this.$el.html(this.template);
      this.modelBinder.bind(this.model, this.$el);
      return this;
    },

    events: {
      submit: 'onCreateMenu'
    },

    onCreateMenu: function (e) {
      e.preventDefault();
      var $button = this.$el.find('button[type="submit"]');

      // TODO: refactor this. use event listeners
      this.sandbox.processNotifier.notify($button);
      this.model.save().then(function () {
        this.sandbox.processNotifier.stop($button);
        this.sandbox.emit('menu-created', this.model);
				$('#myModal').modal('hide');
				this.sandbox.notifier.success(
					this.sandbox.globals.messages.generic.CREATE_MENU.success.message,
					this.sandbox.globals.messages.generic.CREATE_MENU.success.title
				);
      }.bind(this), function () {
        this.sandbox.processNotifier.stop($button);
				this.sandbox.notifier.success(
					this.sandbox.globals.messages.generic.CREATE_MENU.error.message,
					this.sandbox.globals.messages.generic.CREATE_MENU.error.title
				);
			}.bind(this));

    }
  });

  return View;
});
