define(['sentientview', 'text!../templates/toolbox-controls.hbs'], function (SentientView, template) {
  'use strict';

  var AppView = SentientView.extend({

    template: template,

    init: function () {
      this.$el.html(this.template);
      this.initializeToolboxDraggable();
      this.sandbox.start([{
        name: 'toolbox-controls-datasource',
        options: { el: '#datasource-accordion' }
      }]);
    },

    render: function () {
      var self = this;
			self.popItOver();
      return self;
    },

    initializeToolboxDraggable: function () {
      this.$el.find('[draggable=true]').draggable({
        cursor: 'move',
        cursorAt: { top: -5, left: -5 },
        helper: 'clone',
        //revert: true,
        opacity: 0.7,
        connectToSortable: '[sortable=true]',
        zIndex: 9999
      });
    },

		popItOver: function () {
			var self = this,
					popover = this.$el.find('#toolbox-controls-accordion .list-group-item');
			popover.popover({
				html : true,
				placement: 'left',
				trigger: 'hover',
        content: function () {
          var control =  $(this).attr('data-control');
					return self.$el.find('#control-popover-content div[data-ctrl="' + control + '"]').html();
        }
			});
		}

  });

  return AppView;

});
