define(['sentientview', '../models/canvas-designer.model'], function (SentientView, DesignerModel) {
  'use strict';

  var AppView = SentientView.extend({

    tagName: 'div',

    init: function (options) {
      this.widgetDesigner = options.widgetDesigner;
      this.loadDesigner();
      this.designerModel = new DesignerModel();
    },

    render: function () {
      var self = this;
      return self;
    },

    loadDesigner: function () {
      this.$el.html(this.widgetDesigner.designerModel.get('contents'));
    }

  });

  return AppView;

});
