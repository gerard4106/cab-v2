define(['backbone'], function (Backbone) {
  'use strict';

  var DesignerModel = Backbone.Model.extend({
    defaults: {}
  });

  return DesignerModel;

});