define(['./views/canvas-designer'], function (AppView) {
  'use strict';

  return {

    initialize: function () {
      var app = new AppView({ sandbox: this.sandbox, widgetDesigner: this.options.params });
      this.html(app.render().el);
    }

  };

});
