define(['sentientview', 'text!../templates/popup.hbs'], function (SentientView, template) {
	'use strict';

	var AppView = SentientView.extend({

		template: template,
    isGlobalWidget: true,

		init: function () {
			this.$el.html(this.template);
		},

		render: function () {
			var self = this;
			return self;
		}

	});

	return AppView;
});
