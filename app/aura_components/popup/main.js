define(['./views/app'], function (AppView) {
  'use strict';

  return {

    initialize: function () {
      var app = new AppView({ sandbox: this.sandbox });
      $('#cab-popup').html(app.render().el);
    }

  };

});

