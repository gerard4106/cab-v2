define(['./views/app'], function (AppView) {
  'use strict';

  return {

    initialize: function () {
      this.initializeHeaderView();
    },

    initializeHeaderView: function () {
      var self = this,
        app = new AppView({ sandbox: self.sandbox });
      $('#cab-footer').html(app.render().el);
    }

  };

});

