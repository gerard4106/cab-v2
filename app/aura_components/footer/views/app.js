define(['sentientview', 'text!../templates/footer.hbs'], function (SentientView, template) {
	'use strict';

	var HeaderView = SentientView.extend({

		template: template,

		initialize: function (options) {
			var self = this;
			self.sandbox = options.sandbox;
			self.$el.html(self.template);
		},

		render: function () {
			var self = this;
			return self;
		}

	});

	return HeaderView;
});