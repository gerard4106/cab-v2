define(['sentientview', 'text!../templates/toolbox-properties-effects.hbs'], function (SentientView, template) {
  'use strict';

  var AppView = SentientView.extend({

    template: template,

    init: function () {
      this.$el.html(this.template);
    },

    render: function () {
      return this;
    }

  });

  return AppView;

});