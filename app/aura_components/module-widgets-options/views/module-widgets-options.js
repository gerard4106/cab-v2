define(['sentientview', 'text!../templates/module-widgets-options.hbs'], function (SentientView, template) {
  'use strict';

  var AppView = SentientView.extend({

    template: template,

    init: function () {
      this.$el.html(this.template);
    },

    render: function () {
      var self = this;
      return self;
    },

    events: {
      'click #add-new-widget': 'addNewWidget'
    },

    addNewWidget: function () {
      var componentName = 'module-widgets-add';
      this.showWidgetOnModal(componentName);
    }

  });

  return AppView;

});

