define(['sentientview', 'text!../templates/header.hbs'], function (SentientView, template) {
	'use strict';

	var HeaderView = SentientView.extend({

		template: template,
		isGlobalWidget: true,
		init: function () {
			this.$el.html(this.template);
			this.designerTabs = this.$el.find('#designer-tabs');
			this.initializeSubscriptions();
			this.changeTheme();
			this.addHeaderToCache();
		},

		render: function () {
			var self = this;
			return self;
		},

		events: {
			'click #projectDatasource' : 'showDatasourceModal',
			'click #datasource': 'showMashableDataSource',
			'click #project-name': 'onClickProjName'
		},

		addHeaderToCache: function () {
			this.sandbox.webStorage.sessionStorage.setItem('isHeaderLoaded', true);
		},

		onClickProjName: function (e) {
			e.preventDefault();
			if (this.projectData) {
				window.location.hash = '#appbuilder/' + this.projectData.id;
			} else {
				this.showNewProjectModal();
			}
		},

		showNewProjectModal: function () {
      var componentName = 'dashboard-new-project';
      this.showWidgetOnModal(componentName);
		},

		initializeSubscriptions: function () {
			this.sandbox.on('add-designer-tab', this.addDesignerTab, this);
      this.sandbox.on('set-active-tab', this.setActiveTab, this);
      this.sandbox.on('on-load-project', this.checkIfProjectDataExisting, this);
		},

		checkIfProjectDataExisting: function () {
			var projectName = this.$el.find('#project-name');
			this.projectData = JSON.parse(this.sandbox.webStorage.sessionStorage.getItem('project-data'));
			if (this.projectData) {
				projectName.text(this.projectData.name);
			}
		},

		changeTheme: function () {
			var self = this;
			$(self.$el).find('#theme-light').click(function (e) {
        e.preventDefault();
				$('body').removeClass('theme-dark').addClass('theme-light');
      });
			$(self.$el).find('#theme-dark').click(function (e) {
        e.preventDefault();
				$('body').removeClass('theme-light').addClass('theme-dark');
      });
		},

    setActiveTab: function (id) {
      this.designerTabs.find('li').removeClass('active');
      this.designerTabs.find('[href=#' + id + ']').parent().addClass('active');
    },

		addDesignerTab: function (data) {
			var displayName = data.data.widgetNameSlug + '/templates/' + data.data.fileName,
				li = $('<li class="active"></li>'),
				a = $('<a href="#' + data.id + '" data-toggle="tab">' + displayName + '</a>'),
				i = '<i class="fa fa-times remove-pane"></i>';
			a.append(i);
			li.append(a);
			this.designerTabs.children().removeClass('active');
			this.designerTabs.append(li);
      var close = this.designerTabs.find('[href=#' + data.id + ']');
      this.bindCloseActiveTab(close);
		},

    bindCloseActiveTab: function (close) {
      var self = this;

      if (close) {
        close = self.designerTabs.find('a > i');
      }

      close.on('click', function () {
        var id = $(this).parent().attr('href'),
					li = $(this).parents('li');
					//confirmClose = false;

				/*confirmClose = window.confirm('Are you sure you want to close this tab?');

				if (confirmClose) {
					//if (self.designerTabs.find('li').length > 1) {
					self.designerTabs.find('li:first').addClass('active');
					//}
					self.sandbox.emit('remove-designer', id);
					li.remove();
				}*/
        self.designerTabs.find('li:first').addClass('active');
        //}
        self.sandbox.emit('remove-designer', id);
        li.remove();
      });
    },

		showDatasourceModal: function (e) {
			e.preventDefault();
			//var componentName = 'project-datasource';
      var componentName = 'project-datasource-configuration';
      this.showWidgetOnModal(componentName);
    },

    showMashableDataSource: function () {
      this.showWidgetOnModal('header-mashable-datasource');
    },

    removeDesignerView: function (closeBtn) {
      //bind remove tab
      closeBtn.on('click', function (e) {
        var id = $(e.target).parent().attr('href').replace('#', '');
        console.log(id);
        //make another implementation
      });
    }

	});

	return HeaderView;
});
