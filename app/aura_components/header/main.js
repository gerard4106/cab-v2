define(['./views/header'], function (HeaderView) {
  'use strict';

  return {

    initialize: function () {
      this.initializeHeaderView();
    },

    initializeHeaderView: function () {
      var self = this,
        headerView = new HeaderView({ sandbox: self.sandbox });
      $('#cab-header').html(headerView.render().el);
    }

  };

});

