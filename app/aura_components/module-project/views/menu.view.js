define(['sentientview', 'text!../templates/menu.template.hbs', '../models/menu-design.model'], function (SentientView, template, MenuDesignerModel) {
  'use strict';

  var View = SentientView.extend({

    tagName: 'li',
    className: 'menu-item',

    template: template,

    init: function () {
      this.template = this.sandbox.compileTemplate(this.template);
      this.projectData = JSON.parse(this.sandbox.webStorage.sessionStorage.getItem('project-data'));

    },

    render: function () {
      this.$el.html(this.template(this.model.toJSON()));
      this.toggle();
      this.initializeContextMenu();
      return this;
    },

    toggle: function () {
      this.$el.find('.menu-file').off('click').click(function (e) {
        e.preventDefault();
      });
    },

    events: {
      'dblclick .menu-file': 'aceShowCode'
    },

    initializeContextMenu: function () {
      var self = this;
      $.contextMenu({
        selector: '[data-tree-header="' + self.model.get('slug') + '"]',
        className: 'dropdown-menu',
        callback: function (key) { // key, options

          if (key === 'view') {
            var projectData = JSON.parse(self.sandbox.webStorage.sessionStorage.getItem('project-data'));
            var data = {
              projectNameSlug: projectData.slug,
              type: self.sandbox.globals.fileTypes.TEMPLATES,
              fileName: self.model.get('slug'),
              classification: self.sandbox.globals.fileClassifications.MENU,
              widgetNameSlug: self.model.get('slug')
            };

            self.designerModel = new MenuDesignerModel();
            self.designerModel.urlRoot = self.sandbox.globals.HOST + 'files';
            self.designerModel.fetch({ data: data }).then(function () {
              var menuData = {
                data: data,
                designerModel: self.designerModel.toJSON(),
                type: 'designer'
              };
              self.sandbox.emit('on-show-designer', menuData);
            });

          }
        },
        items: {
          'rename': {name: 'Rename', icon: 'cut'},
          'view': {name: 'View Designer', icon: 'copy'},
          'sep1': '---------',
          'delete': {name: 'Delete', icon: 'quit'}
        }
      });
    },

    aceShowCode: function (e) {
      var self = this;
      var projectData = JSON.parse(self.sandbox.webStorage.sessionStorage.getItem('project-data'));
      var target = $(e.target),
        parent = target.parents('.collapse.parent-tree'),
        data = {
          projectNameSlug: projectData.slug,
          fileName: target.attr('data-filename'),
          classification: self.sandbox.globals.fileClassifications.MENU,
          widgetNameSlug: parent.attr('id')
        };

      if (target.attr('data-type')) {
        if (self.sandbox.globals.fileTypes.TEMPLATES === target.attr('data-type')) {
          data.type = self.sandbox.globals.fileTypes.TEMPLATES;
        } else if (self.sandbox.globals.fileTypes.VIEWS === target.attr('data-type')) {
          data.type = self.sandbox.globals.fileTypes.VIEWS;
        }
      }

      self.aceModel = new MenuDesignerModel();
      self.aceModel.urlRoot = self.sandbox.globals.HOST + 'files';
      self.aceModel.fetch({ data: data }).then(function () {
        var menuData = {
          data: data,
          aceModel: self.aceModel.toJSON(),
          type: 'code'
        };
        console.log('show code');
        self.sandbox.emit('on-show-designer', menuData);
      });
    },

  });

  return View;
});
