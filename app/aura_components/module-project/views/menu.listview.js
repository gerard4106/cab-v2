define(['sentientview', './menu.view', '../models/menu.model', 'text!../templates/module-project.hbs'], function (SentientView, MenuView, MenuModel, template) {
  'use strict';

  var AppView = SentientView.extend({

    template: template,
    init: function () {
      var self = this;
      self.$el.html(this.template);

      this.listenTo(this.collection, 'reset', this.renderMenus);
      this.listenTo(this.collection, 'add', this.addOne);
      this.sandbox.on('menu-created', this.onCreatedMenu, this);
    },

    render: function () {
      var self = this;
      return self;
    },

    events: {
      'click #add-new-menu': 'onAddMenu'
    },

    renderMenus: function () {

      this.collection.forEach(this.addOne, this);
    },

    addOne: function (menu) {
      var $menuList = this.$el.find('.cab-tree > ol');
      var menuView = new MenuView({ sandbox: this.sandbox, model: menu });
      $menuList.append(menuView.render().el);
    },

    onAddMenu: function () {
      this.showWidgetOnModal('module-project-add');
    },

    onCreatedMenu: function (menu) {
      console.log('onCreatedMenu Pub/sub listener');
      this.collection.add(menu);
    }

  });

  return AppView;

});
