define(['sentientcollection', '../models/menu.model'], function (SentientCollection, Menu) {
  'use strict';

  var Collection = SentientCollection.extend({
    model: Menu,
    url: function () {
      var projectId = this.sandbox.webStorage.sessionStorage.getItem('project-id');
      return this.sandbox.globals.HOST + 'menus/' + projectId;
    },
    parse: function (response) {
      return response.data.menus;
    }
  });

  return Collection;
});
