define(['sentientmodel'], function (SentientModel) {
  'use strict';

  var MenuModel = SentientModel.extend({
    defaults: {},
    parse: function (response) {
      var menuContent = response.data;
      return menuContent;
    }
  });

  return MenuModel;

});