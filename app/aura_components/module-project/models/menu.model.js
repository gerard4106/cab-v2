define(['sentientmodel'], function (SentientModel) {
  'use strict';

  var Model = SentientModel.extend({
    defauts: {
      ParentId: 0,
      name: '',
      description: ''
    }
  });

  return Model;

});
