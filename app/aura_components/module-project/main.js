define(['./views/menu.listview', './collections/menu.collection'], function (MenuListView, MenuCollection) {
  'use strict';

  return {

    initialize: function () {

      var menuCollection = new MenuCollection({ sandbox: this.sandbox });

      console.log('fetching menus', menuCollection);
      menuCollection.fetch({ id: this.sandbox.webStorage.sessionStorage.getItem('project-id') });
      var menuListView = new MenuListView({ sandbox: this.sandbox, collection: menuCollection });
      this.html(menuListView.render().el);
    }

  };

});
