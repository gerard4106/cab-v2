define(['sentientview', 'text!../templates/toolbox-properties-bootstrap.hbs'], function (SentientView, template) {
  'use strict';

  var AppView = SentientView.extend({

    template: template,

    init: function () {
      this.html(this.template);
      this.initializeSubscriptions();
      this.getActiveControl();
    },

    render: function () {
      return this;
    },

    startTestComponent: function () {
      this.sandbox.start([{
        name : 'button@bootstrapProperties',
        options: { el: this.$el }
      }]);
    },

    initializeSubscriptions: function () {
      this.sandbox.on('on-reset-property', this.getActiveControl, this);
    },

    getActiveControl: function () {
      if (this.checkActiveControl()) {
        var control = this.sandbox.canvas.ACTIVE.attr('data-control'),
          componentName = control + '@bootstrapProperties';
        this.initializeControlBoostrapProperty(componentName);
      }
    },

    initializeControlBoostrapProperty: function (name) {
      console.log('on bootstrap properties', name);
      this.sandbox.start([{
        name : name,
        options: { el: this.$el }
      }]);
    }

  });

  return AppView;

});
