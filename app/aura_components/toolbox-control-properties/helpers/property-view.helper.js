/**
 * author: garguelles
 * description: handles rendering of property views
 */

define([
  'underscore',
  '../views/button-dropdown-properties'
], function (_, ButtonDropDownPropertyView) {
  'use strict';

  // key/value pair of cab_control name and coressponding view
  var views = {
    /*jshint camelcase: false */
    button_dropdown: ButtonDropDownPropertyView
  };

  var PropertyViewHelper = function (options) {
    this.settings = _.extend({}, PropertyViewHelper.DEFAULTS, options);
  };

  PropertyViewHelper.DEFAULTS = {
    View: undefined,
    currentView: undefined,
    sandbox: undefined,
    dataControlName: '',
    foucsedControl: undefined
  };

  // expose a setView interface
  PropertyViewHelper.prototype.setView = function (dataControlName) {
    if (views[dataControlName]) {
      this.settings.View = views[dataControlName];
    }
  };

  PropertyViewHelper.prototype.focusedControl = function (control) {
    this.settings.focusedControl = control;
  };

  PropertyViewHelper.prototype.CurrentView = undefined;

  PropertyViewHelper.prototype.render = function ($el) {
    var ControlView = this.settings.View,
    options = { // view options
      //el: $el
    };

    if (this.settings.currentView) {
      this.settings.currentView.remove();
    }

    if (this.settings.focusedControl) {
      options.focusedControl = this.settings.focusedControl;
    } else {
      // TODO: garguelles: should I throw an error?
    }

    if (this.settings.sandbox) {
      // find a way to create a new sandbox - may aura object global?
      //options.sandbox = this.settings.sandbox.create();
      options.sandbox = this.settings.sandbox;
    }
    this.settings.currentView = new ControlView(options);
    $el.html(this.settings.currentView.render().el);
    //this.settings.currentView = new ControlView(options).render();
  };

  return PropertyViewHelper;

});
