define(['./views/control-properties'], function (ControlPropertiesView) {
  'use strict';

  return {

    initialize: function () {

      var controlPropertiesView = new ControlPropertiesView({ sandbox: this.sandbox });
      this.html(controlPropertiesView.render().el);
    }

  };

});
