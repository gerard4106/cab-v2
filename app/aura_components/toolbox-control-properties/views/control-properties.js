define([
  'sentientview',
  'text!../templates/control-properties.template.hbs',
  '../helpers/property-view.helper'
], function (SentientView, template, PropertyViewHelper) {
  'use strict';

  // TODO: create a template and view for each control
  var View = SentientView.extend({

    template: template,
    init: function () {

      this.propertyViewHelper = new PropertyViewHelper({
        sandbox: this.sandbox
      });

      this.sandbox.on('on-reset-property', this.renderControlProperties, this);
    },

    render: function () {

      this.$el.html(this.template);
      console.log('the html', this.$el, this.$el.html());
      this.$propertiesContainer = this.$el.find('div.control-property-container');

      // call on render - this.$propertiesContainer must be set first
      this.renderControlProperties();

      return this;
    },

    renderControlProperties: function () {
      console.log('render control properties');
      var controlName;
      if (this.checkActiveControl()) {
        controlName = this.focusedControl.closest('div[data-control]').data('control');
        this.propertyViewHelper.setView(controlName);
        this.propertyViewHelper.focusedControl(this.focusedControl);
        this.propertyViewHelper.render(this.$propertiesContainer);
      }
    }
  });

  return View;

});
