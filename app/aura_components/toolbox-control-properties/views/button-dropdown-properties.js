define([
  'sentientview',
  'text!../templates/button-dropdown-properties.template.hbs'
], function (SentientView, template) {
  'use strict';

  var View = SentientView.extend({

    template: template,

    init: function (options) {

      this.$el.html(this.template);
      this.control = options.focusedControl;
      this.$staticList = this.$el.find('#static-list');
      this.$item = this.$el.find('#static-item');

    },

    render: function () {
      this.retrieveStaticItems();
      return this;
    },

    events: {
      submit: 'addStaticItem'
    },

    addStaticItem: function (e) {
      e.preventDefault();
      var $li = this.createElement(this.$item.val());
      this.appendToListProperty($li.clone());
      this.appendToActiveControl($li);
    },

    createElement: function (item) {
      var $a = $('<a href="#"></a>').html(item);
      var $li = $('<li class="static-list-item"></li>').html($a);
      return $li;
    },

    appendToActiveControl: function (item) {
      this.control.find('ul.dropdown-menu').append(item);
      this.$item.val('');
    },

    appendToListProperty: function (item) {
      $(item).appendTo(this.$staticList);
    },

    retrieveStaticItems: function () {
      var staticItems = this.control.find('li.static-list-item');
      console.log('retrieving static items', staticItems);
      _.each(staticItems, function (value) {
        this.appendToListProperty(value);
      }.bind(this));
    }

  });

  return View;

});
