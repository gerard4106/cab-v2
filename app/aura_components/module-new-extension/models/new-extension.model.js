define(['sentientmodel'], function (SentientModel) {
  'use strict';

  var Model = SentientModel.extend({
    urlRoot: function () {
      return this.sandbox.globals.HOST + 'extensions';
    },

    defaults: {
      name: '',
      projectId: null,
      description: '',
      projectNameSlug: ''
    },

    parse: function (response) {
      return response.data.createdExtension;
    }
  });

  return Model;

});
