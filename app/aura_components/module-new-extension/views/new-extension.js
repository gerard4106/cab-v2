define(['sentientview', 'text!../templates/new-extension.hbs'], function (SentientView, template) {
  'use strict';

  var View = SentientView.extend({
    template: template,

    init: function () {

    },

    render: function () {
      this.$el.html(this.template);
      return this;
    },

    events: {
      'click #create-new-extension' : 'createNewExtension'
    },

    createNewExtension: function (e) {
      var self = this;
      var name = this.$el.find('#ExtensionName').val();
      self.model.set('name', name);
      this.sandbox.processNotifier.notify($(e.currentTarget));
      self.model.save().then(function () {
        self.sandbox.processNotifier.stop($(e.currentTarget));
        self.sandbox.emit('extension-created', self.model);
        $('#myModal').modal('hide');
        self.sandbox.notifier.success(
          self.sandbox.globals.messages.generic.CREATE_EXTENSION.success.title,
          self.sandbox.globals.messages.generic.CREATE_EXTENSION.success.message
        );
      }, function (err) {
        self.sandbox.logger('request error', err);
        self.sandbox.notifier.error(self.sandbox.messages.generic.CREATE_EXTENSION.error.title,
          self.sandbox.messages.generic.CREATE_EXTENSION.error.message);
      });
    }

  });

  return View;
});
