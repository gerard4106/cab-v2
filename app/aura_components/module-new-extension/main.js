define(['./views/new-extension', './models/new-extension.model'], function (ExtensionView, ExtensionModel) {
  'use strict';

  return {

    initialize: function () {

      var session = this.sandbox.webStorage.sessionStorage;

      var menu = new ExtensionModel({ sandbox: this.sandbox, projectId: session.getItem('project-id'), projectNameSlug: session.getItem('projectNameSlug') });
      var menuView = new ExtensionView({ sandbox: this.sandbox, model: menu });

      this.html(menuView.render().el);
    }
  };

});
