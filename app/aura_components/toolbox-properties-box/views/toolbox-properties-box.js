define(['sentientview', 'text!../templates/toolbox-properties-box.hbs', '../models/properties-box-model'], function (SentientView, template, BoxModel) {
  'use strict';

  var AppView = SentientView.extend({

    template: template,

    init: function () {
      this.$el.html(this.template);
      this.initializeSubscriptions();
      this.getElementStyle();
    },

    render: function () {
      return this;
    },

    events: {
      'blur #control-border-width .border-width' : 'onBorderWidthChange',
      'blur #control-border-radius .border-radius' : 'onBorderRadiusChange',
      'blur #control-padding .padding' : 'onBorderPaddingChange',
      'blur #control-margin .margin' : 'onBorderMarginChange'
    },

    initializeSubscriptions: function () {
      this.sandbox.on('on-reset-property', this.getElementStyle, this);
    },

    getElementStyle: function () {
      var self = this;
      if (self.checkActiveControl()) {
        var control = self.focusedControl,
          layoutModel = {
            'padding' : control.css('padding').replace('px', ''),
            'pTop' : control.css('padding-top').replace('px', ''),
            'pLeft' : control.css('padding-left').replace('px', ''),
            'pRight' : control.css('padding-right').replace('px', ''),
            'pBottom' : control.css('padding-bottom').replace('px', ''),
            'margin' : control.css('margin').replace('px', ''),
            'mTop' : control.css('margin-top').replace('px', ''),
            'mLeft' : control.css('margin-left').replace('px', ''),
            'mRight' : control.css('margin-right').replace('px', ''),
            'mBottom' : control.css('margin-bottom').replace('px', ''),
            'radius' : control.css('border-radius').replace('px', ''),
            'rTopLeft' : control.css('border-top-left-radius').replace('px', ''),
            'rTopRight' : control.css('border-top-right-radius').replace('px', ''),
            'rBotLeft' : control.css('border-bottom-left-radius').replace('px', ''),
            'rBotRight' : control.css('border-bottom-right-radius').replace('px', ''),
            'border' : control.css('border').replace('px', ''),
            'bTop' : control.css('border-top-width') === '' ? '0' : control.css('border-top-width').replace('px', ''),
            'bLeft' : control.css('border-left-width') === '' ? '0' : control.css('border-left-width').replace('px', ''),
            'bRight' : control.css('border-right-width') === '' ? '0' : control.css('border-right-width').replace('px', ''),
            'bBottom' : control.css('border-bottom-width') === '' ? '0' : control.css('border-bottom-width').replace('px', '')
          },
          LayoutModel = new BoxModel(layoutModel);
        self.modelBinder.bind(LayoutModel, self.$el);
      }
    },

    onBorderWidthChange: function (e) {
      if (this.checkActiveControl()) {
        var val = $(e.target).val(),
          placement = $(e.target).attr('placement');
        this.focusedControl.css(placement, val + 'px solid');
        if (placement === 'border') {
          this.$el.find('#control-border-width input').val(val);
        }
      }
    },

    onBorderRadiusChange: function (e) {
      if (this.checkActiveControl()) {
        var val = $(e.target).val(),
          placement = $(e.target).attr('placement');
        this.focusedControl.css(placement, val + 'px');
        if (placement === 'border-radius') {
          this.$el.find('#control-border-radius input').val(val);
        }
      }
    },

    onBorderPaddingChange: function (e) {
      if (this.checkActiveControl()) {
        var val = $(e.target).val(),
          placement = $(e.target).attr('placement');
        this.focusedControl.css(placement, val + 'px');
        if (placement === 'padding') {
          this.$el.find('#control-padding input').val(val);
        }
      }
    },

    onBorderMarginChange: function (e) {
      if (this.checkActiveControl()) {
        var val = $(e.target).val(),
          placement = $(e.target).attr('placement');
        this.focusedControl.css(placement, val + 'px');
        if (placement === 'margin') {
          this.$el.find('#control-margin input').val(val);
        }
      }
    }

  });

  return AppView;

});