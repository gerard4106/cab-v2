define(['backbone'], function (Backbone) {
  'use strict';

  var BoxModel = Backbone.Model.extend({
    defaults: {}
  });

  return BoxModel;

});