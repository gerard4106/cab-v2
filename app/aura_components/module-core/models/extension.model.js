define(['sentientmodel'], function (SentientModel) {
  'use strict';

  var Model = SentientModel.extend({
    defaults: {
      name: '',
      description: '',
      projectId: null,
      projectNameSlug: ''
    }
  });

  return Model;

});
