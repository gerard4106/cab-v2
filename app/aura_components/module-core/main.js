define(['./views/module-core', './models/extension.model'], function (AppView, Model) {
  'use strict';

  return {

    initialize: function () {
      var model = new Model();
      var app = new AppView({ sandbox: this.sandbox, model: model });
      this.html(app.render().el);
    }

  };

});
