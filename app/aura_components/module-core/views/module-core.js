define([
  'sentientview',
  'text!../templates/module-core.hbs',
  '../collections/extensions.collection',
  '../views/extension.list-view',
  '../models/core.model'
], function (SentientView, template, ExtensionsCollection, ExtensionListView, CoreModel) {
  'use strict';

  var AppView = SentientView.extend({

    template: template,

    init: function () {

      this.extensions = new ExtensionsCollection({ sandbox: this.sandbox });

    },

    render: function () {
      this.$el.html(this.template);

      this.extensionListView = new ExtensionListView({
        sandbox: this.sandbox,
        collection: this.extensions,
        el: this.$el.find('#core-extensions-panel')
      });

      this.extensionListView.render();

      this.initializeContextMenu();

      return this;
    },

    events: {
      'click a.core-file': 'preventTrigger',
      'dblclick a.core-file': 'showContents'
    },

    preventTrigger: function (e) {
      e.preventDefault();
    },

    initializeContextMenu: function () {
      var self = this;
      $.contextMenu({
        selector: '#core-extensions-panel',
        className: 'dropdown-menu',
        callback: function (key) { // key, options
          if (key === 'new_extension') {
            self.showWidgetOnModal('module-new-extension');
          }
        },
        items: {
          'new_extension': {name: 'Create New Extension', icon: 'edit'}
        }
      });
    },

    showContents: function (e) {
      e.preventDefault();
      var projectData = JSON.parse(this.sandbox.webStorage.sessionStorage.getItem('project-data'));
      var target = $(e.target),
        //parent = target.parents('.collapse.parent-tree'),
        fileName = target.attr('data-filename');
      this.data = {
        projectNameSlug: projectData.slug,
        //type: this.sandbox.globals.fileTypes.VIEWS,
        fileName: fileName,
        classification: this.sandbox.globals.fileClassifications.CORE
      };

      if (fileName === 'style.less') {
        this.data.type = 'assets';
      } else if (fileName === 'index.html') {
        this.data.type = 'templates';
      } else if (fileName === 'router.js' || fileName === 'main.js') {
        this.data.type = 'javascript';
      }

      this.coreModel = new CoreModel();

      this.listenTo(this.coreModel, 'change', this.onCoreFileFetch);

      this.coreModel.urlRoot = this.sandbox.globals.HOST + 'files';
      this.coreModel.fetch({ data : this.data });
    },

    onCoreFileFetch: function () {
      var coreData = {
        data: this.data,
        aceModel: this.coreModel.toJSON(),
        type: 'code'
      };
      this.sandbox.emit('on-show-designer', coreData);
    }

  });

  return AppView;
});
