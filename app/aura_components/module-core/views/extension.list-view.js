define([
  'sentientview',
  'text!../templates/extensions.template.hbs',
  './extension.view',
], function (SentientView, template, ExtensionView) {
  'use strict';

  var View = SentientView.extend({
    template: template,

    init: function () {
      //var projectId = this.sandbox.webStorage.localStorage.getItem('project-id');
      this.listenTo(this.collection, 'add', this.addOne);
      this.listenTo(this.collection, 'reset', this.renderAll);
      this.sandbox.on('extension-created', this.onExtensionCreated, this);
    },

    render: function () {
      this.$el.html(this.template);
      this.$extensionList = this.$el.find('#extension-list');

      this.collection.fetch();
      this.renderAll();
    },

    renderAll: function () {
      this.collection.forEach(this.addOne, this);
    },

    addOne: function (model) {
      var extensionView = new ExtensionView({ sandbox: this.sandbox, model: model });
      this.$extensionList.append(extensionView.render().el);
    },

    onExtensionCreated: function (model) {
      this.collection.add(model);
    }
  });

  return View;

});
