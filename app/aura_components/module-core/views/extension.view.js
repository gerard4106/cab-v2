define(['sentientview', '../models/extension.model'], function (SentientView, ExtensionModel) {
  'use strict';

  var View = SentientView.extend({

    tagName: 'li',

    template: '<i class="fa-li fa fa-folder fa-fw"></i><a href="#">{{name}}</a>',

    init: function () {
      this.template = this.sandbox.compileTemplate(this.template);

      this.listenTo(this.model, 'remove', this.remove);
      this.listenTo(this.model, 'change', this.render);
      //this.listenTo(this.model, 'reset', this.render);
    },

    render: function () {
      this.$el.html(this.template(this.model.attributes));
      return this;
    },

    events: {
      'click a': 'preventTrigger',
      'dblclick': 'aceShowCode'
    },

    preventTrigger: function (e) {
      e.preventDefault();
    },

    aceShowCode: function () {
      var projectData = JSON.parse(this.sandbox.webStorage.sessionStorage.getItem('project-data'));
      this.extensionData = {
        projectNameSlug: projectData.slug,
        fileName: this.model.get('name'),
        classification: 'extension'
      };

      this.extensionModel = new ExtensionModel();

      this.listenTo(this.extensionModel, 'change', this.onContentFetch);

      this.extensionModel.urlRoot = this.sandbox.globals.HOST + 'files';
      this.extensionModel.fetch({ data: this.extensionData });
    },

    onContentFetch: function () {
      var extensionData = this.extensionModel.get('data');
      var extension = {
        data: this.extensionData,
        aceModel: extensionData,
        type: 'code'
      };
      this.sandbox.emit('on-show-designer', extension);
    }

  });

  return View;

});
