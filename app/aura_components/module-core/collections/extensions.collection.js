define(['sentientcollection', '../models/extension.model'], function (SentientCollection, ExtensionModel) {
  'use strict';

  var Collection = SentientCollection.extend({
    model: ExtensionModel,
    url: function () {
      var projectId = this.sandbox.webStorage.sessionStorage.getItem('project-id');
      return this.sandbox.globals.HOST + 'extensions/' + projectId;
    },
    parse: function (response) {
      return response.data.extensions;
    }
  });

  return Collection;

});



