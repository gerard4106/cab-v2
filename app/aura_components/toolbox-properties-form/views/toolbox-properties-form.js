define(['sentientview', 'text!../templates/toolbox-properties-form.hbs'], function (SentientView, template) {
  'use strict';

  var AppView = SentientView.extend({

    template: template,

    init: function () {
      this.html(this.template);
    },

    render: function () {
      return this;
    },

    events: {
      'change #form-page-layouts': 'onChangeLayout'
    },

    onChangeLayout: function (e) {
      var val = $(e.target).val(),
        previewPanes = this.$el.find('#page-layout-preview > .tab-pane.active');

      previewPanes.removeClass('active');
      this.$el.find('#page-layout-preview > #' + val).addClass('active');
    }

  });

  return AppView;

});