define(['sentientmodel'], function (SentientModel) {
  'use strict';

  var WidgetModel = SentientModel.extend({
    defaults: {
      name: '',
      instance: 'single'
    },

    parse: function (response) {
      return response.data.createdWidget;
    }

  });

  return WidgetModel;

});
