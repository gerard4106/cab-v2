define(['sentientview', 'text!../templates/module-widgets-add.hbs'], function (SentientView, template) {
  'use strict';

  var AppView = SentientView.extend({

    template: template,

    init: function () {
      this.$el.html(this.template);
      this.modelBinder.bind(this.model, this.$el);
    },

    render: function () {
      var self = this;
      return self;
    },

    events: {
      submit : 'createNewWidget'
    },

    createNewWidget: function (e) {
      var self = this,
        $el = this.$el.find('button[type="submit"]');

      e.preventDefault();
      var projectData = $.parseJSON(self.sandbox.webStorage.sessionStorage.getItem('project-data')),
        projectId = projectData.id,
        projectNameSlug = projectData.slug;
        //widgetName = self.$el.find('#NewWidgetName').val();

      this.sandbox.processNotifier.notify($el);

      this.model.set({
        //name: widgetName,
        projectNameSlug: projectNameSlug,
        projectId: projectId
      });

      this.model.urlRoot = self.sandbox.globals.HOST + 'widgets';
      this.model.save().then(function () {
        console.log('this.createdmodel', this.model.toJSON());
        this.sandbox.emit('widget-created', this.model);
        this.sandbox.processNotifier.stop($el);
        $('#myModal').modal('hide');
				this.sandbox.notifier.success(
					this.sandbox.globals.messages.generic.CREATE_WIDGET.success.message,
					this.sandbox.globals.messages.generic.CREATE_WIDGET.success.title
				);
      }.bind(this), function () {
				this.sandbox.notifier.success(
					this.sandbox.globals.messages.generic.CREATE_WIDGET.error.message,
					this.sandbox.globals.messages.generic.CREATE_WIDGET.error.title
				);
			});
    }

  });

  return AppView;

});

