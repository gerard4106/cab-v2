define(['./views/module-widgets-add', './models/new-widget.model'], function (AppView, Widget) {
  'use strict';

  return {

    initialize: function () {

      var widget = new Widget({ sandbox: this.sandbox });
      var app = new AppView({ sandbox: this.sandbox, model: widget });

      this.html(app.render().el);
    }

  };

});
