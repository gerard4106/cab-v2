define(['sentientview', 'text!../templates/loading.hbs'], function (SentientView, template) {
	'use strict';

	var LoadingView = SentientView.extend({

		template: template,

		init: function () {
			this.$el.html(this.template);
			this.parentLoader = $('[data-aura-component="loading"]');
			this.initializeSubscriptions();
		},

		render: function () {
			var self = this;
			return self;
		},

		initializeSubscriptions: function () {
			this.sandbox.on('on-add-loading', this.addLoading, this);
			this.sandbox.on('on-remove-loading', this.removeLoading, this);
		},

		addLoading: function () {
			this.parentLoader.removeClass('hidden');
			console.log(this.parentLoader);
		},

		removeLoading: function () {
			console.log(this.parentLoader);
			this.parentLoader.addClass('hidden');
		}

	});

	return LoadingView;
});