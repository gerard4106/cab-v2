define(['sentientview', 'text!../templates/module.hbs'], function (SentientView, template) {
  'use strict';

  var AppView = SentientView.extend({

    template: template,
    isWidgetsStarted: false,
    init: function () {
      var self = this;
      self.selectors = self.sandbox.globals.selectors;
      self.$el.html(this.template);
      self.projectData = self.sandbox.webStorage.sessionStorage.getItem('project-type');
      self.setAppBuilderMode();
    },

    events: {
      /*'click a[href="#module-widgets"]': 'renderModuleWidgets',
        'click a[href="#module-core"]': 'renderModuleCore'*/
    },

    render: function () {
      return this;
    },

    setAppBuilderMode: function () {
      var self = this,
        $body = $('body');

      switch (self.projectData) {
      case 'app':
        self.initializeWidgets();
        self.initializeTabEventsAppMode();
        $body.attr('data-project-type', 'app');
        break;
      case 'widget':
        self.renderModuleWidgets();
        $body.attr('data-project-type', 'widget');
        break;
      }
    },

    initializeTabEventsAppMode: function () {
      var self = this;
      self.$el.find('a[href="#module-core"]').on('click', this.renderModuleCore.bind(this));
      self.$el.find('a[href="#module-widgets"]').on('click', this.renderModuleWidgets.bind(this));
    },

    initializeWidgets: function () {
      var self = this;
      // start module-projects and module-view only
      // start module-widgets on demand
      self.sandbox.start([{
        name : 'module-project',
        options: { el: self.selectors.leftPanel.PROJECT }
      }, {
        name : 'module-view',
        options: { el: self.selectors.leftPanel.VIEW }
      }]);
    },

    renderModuleWidgets: function () {
      var self = this;

      // module-widgets must onle be loaded once (on demand)
      // after the initial load. new widgets are loaded dynamically or through pub/sub
      if (!this.isWidgetsStarted) {
        self.sandbox.start([{
          name: 'module-widgets',
          options: { el: self.selectors.leftPanel.WIDGETS }
        }, {
          name: 'module-widgets-options',
          options: { el: '#module-widgets-options' }
        }]);

        this.isWidgetsStarted = true;
      }
    },

    renderModuleCore: _.once(function () {
      this.sandbox.start([{
        name: 'module-core',
        options: { el: '#module-core' }
      }]);
    })

  });

  return AppView;

});
