define(['sentientview', 'text!../templates/module-view.hbs'], function (SentientView, template) {
  'use strict';

  var AppView = SentientView.extend({

    template: template,
    init: function () {
      var self = this;
      self.$el.html(this.template);
    },

    render: function () {
      var self = this;
      return self;
    },

    events: {
      'click ul#designer-view-toggle a': 'onChangeDesignerView'
    },

    onChangeDesignerView: function (e) {
      var view = $(e.currentTarget).data('view');
      this.sandbox.emit('on-change-designer-view', view);
    }

  });

  return AppView;

});
