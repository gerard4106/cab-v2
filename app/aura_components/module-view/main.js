define(['./views/app'], function (AppView) {
  'use strict';

  return {

    initialize: function () {
      var selectors = this.sandbox.globals.selectors;
      var app = new AppView({ sandbox: this.sandbox });
      $(selectors.leftPanel.VIEW).html(app.render().el);
    }

  };

});