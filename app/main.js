require.config({
  waitSeconds: 30,
  paths: {
    scripts: 'scripts',
    eventemitter: 'bower_components/eventemitter2/lib/eventemitter2',
    aura: 'bower_components/aura/lib',
    jquery: 'bower_components/jquery/jquery',
    underscore: 'bower_components/underscore/underscore',
    backbone: 'bower_components/backbone/backbone',
    modelBinder: 'bower_components/backbone.modelbinder/Backbone.ModelBinder.min',
    bootstrap: 'bower_components/bootstrap/dist/js/bootstrap.min',
    ace: 'bower_components/ace-builds/src-min-noconflict/ace',
    jqueryuiCore: 'bower_components/jquery.ui/ui/jquery.ui.core',
    jqueryuiMouse: 'bower_components/jquery.ui/ui/jquery.ui.mouse',
    jqueryuiWidget: 'bower_components/jquery.ui/ui/jquery.ui.widget',
    jqueryuiSortable: 'bower_components/jquery.ui/ui/jquery.ui.sortable',
    jqueryuiDraggable: 'bower_components/jquery.ui/ui/jquery.ui.draggable',
    handlebars: 'bower_components/handlebars/handlebars.min',
    colorpicker: 'extensions/jQuery-ColorPicker/colorpicker.min',
    contextmenu: 'bower_components/jQuery-contextMenu/src/jquery.contextMenu',
    jqueryuiPosition: 'bower_components/jQuery-contextMenu/src/jquery.ui.position',
    toastr: 'bower_components/toastr/toastr.min',

    htmlbeautify: 'scripts/beautify-html',
    jsbeautify: 'scripts/beautify',
    cssbeautify: 'scripts/beautify-css',
    /** Sentient Base Classes **/

    sentientview: 'base/base-view',
    sentientmodel: 'base/base-model',
    sentientcollection: 'base/base-collection',
    sentientpolicies: 'base/base-policies',
    sentientservices: 'base/base-services'
  },

  shim: {
    'backbone': {
      deps: ['underscore', 'jquery'],
      exports: 'Backbone'
    },

    'modelBinder': {
      deps: ['backbone']
    },

    'handlebars': {
      exports: 'Handlebars'
    },

    'htmlbeautify': {
      exports: 'html_beautify'
    },

    'bootstrap': {
      deps: ['jquery'],
      exports: '$.fn.accordion'
    },

    'ace': {
      exports: 'ace'
    },

    'jqueryuiCore': {
      deps: ['jquery'],
      exports: '$.fn.core'
    },

    'jqueryuiMouse': {
      deps: ['jquery', 'jqueryuiWidget'],
      exports: '$.fn.mouse'
    },

    'jqueryuiWidget': {
      deps: ['jquery'],
      exports: '$.widget'
    },

    'jqueryuiSortable': {
      deps: ['jquery', 'jqueryuiCore', 'jqueryuiMouse', 'jqueryuiWidget'],
      exports: '$.fn.sortable'
    },

    'jqueryuiDraggable': {
      deps: ['jquery', 'jqueryuiCore', 'jqueryuiMouse', 'jqueryuiWidget'],
      exports: '$.fn.draggable'
    },

    'colorpicker': {
      deps: ['jquery'],
      exports: '$.fn.ColorPicker'
    },

    'jqueryuiPosition': {
      deps: ['jquery'],
      exports: '$.fn.position'
    },

    'contextmenu': {
      deps: ['jquery', 'jqueryuiPosition'],
      exports: '$.fn.contextMenu'
    }
  }
});

/*
 * load bootstrap js on app start
* */

// dont add your scripts here - causes build error - garguelles
require(['aura/aura'], function (Aura) {
  'use strict';

  var app = new Aura({ debug: { enable : true } });

  /*
  Add your extensions here.
  app.use('extensions/sample');
  */

  app.use('extensions/web-storage');
  app.use('extensions/template-compiler');
  app.use('extensions/http');
  app.use('extensions/globals');
  app.use('extensions/canvas');
  app.use('extensions/dispatcher');
  app.use('extensions/formatter');
  app.use('extensions/process-notifier');
  app.use('extensions/notifier');

  app.components.addSource('bootstrapProperties', 'bootstrap_components');

  app.start({ components: 'body' }).then(function () {
    console.log('Aura started...');
  });

});
