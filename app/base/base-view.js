define(['backbone', 'modelBinder', 'ace', 'base/control-templates'], function (Backbone, ModelBinder, ace, controlTemplates) {
  'use strict';

  /*Backbone.View.prototype.close = function () {
    this.sandbox.stopListening();
    this.undelegateEvents();
    this.unbind();
    this.remove();
  };*/

  var View = Backbone.View.extend({

    initialize: function (options) {
      this.html = options.html;
      this.aceEditor = ace;
      if (options.sandbox) {
        this.sandbox = options.sandbox || {};
      }
      this.modelBinder = new ModelBinder();
      this.init(options);
      this.selectors = options.sandbox.globals.selectors;

      if (!this.isGlobalWidget) {
        this.sandbox.on('undelegate', this.close, this);
      }

      window.onbeforeunload = this.removeSession();

    },

    removeSession: _.once(function () {
      this.sandbox.webStorage.sessionStorage.removeItem('isHeaderLoaded');
    }),

    //check current active control
    checkActiveControl: function () {
      if (this.sandbox.canvas.ACTIVE) {
        this.focusedControl = this.sandbox.canvas.ACTIVE.find('.cab-control');
        return true;
      } else {
        return false;
      }
    },

    showWidgetOnModal: function (componentName) {
      var auraComponent = '<div data-aura-component="' + componentName + '"></div>',
        modal = $('#myModal');
      modal.html(auraComponent);
      this.sandbox.start('#myModal');
      modal.modal('show');
    },

    showViewOnModal: function (view) {
      var modal = $('#myModal');
      modal.html(view.render().el);
      modal.modal('show');
    },

    close: function () {
      this.sandbox.stopListening();
      this.remove();
    },

    addButtonLoading: function (el, caption, icon) {
      console.log(el, caption);
      el.text('<i class="fa fa-' + icon + ' fa-spin"></i>&nbsp;&nbsp;' + caption);
      el.attr('disabled', 'disabled');
    },

    removeButtonLoading: function (el, caption) {
      el.text(caption);
      el.removeAttr('disabled', 'disabled');
    },

    checkExistingHeader: function () {
      var self = this;
      var isHeaderExisting = this.sandbox.webStorage.sessionStorage.getItem('isHeaderLoaded');
      if (!isHeaderExisting) {
        this.sandbox.start([{
          name: 'header',
          options: { el: '#cab-header' }
        }]);
      }
    },

    preventDefault: function () {
      setTimeout(function () {
        var designerButtons = $('#cab-design').find('a.cab-control');
        designerButtons.unbind('click');
        designerButtons.on('click', function (e) {
          e.preventDefault();
        });
      }, 500);
    }

  });

  return View;
});
