define(['backbone'], function (Backbone) {
  'use strict';

  var BaseModel = Backbone.Model.extend({
    initialize: function (options) {
      if (options) {
        this.sandbox = options.sandbox || {};
      }

      if (this.init) this.init(options);
    },

    toJSON: function () {
      // remove sandbox from attributes
      var attrs = _.clone(this.attributes);
      if (attrs.hasOwnProperty('sandbox')) {
        delete attrs['sandbox'];
      }
      return attrs;
    }

  });


  return BaseModel;
});
