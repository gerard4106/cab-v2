define(['backbone'], function (Backbone) {
  'use strict';

  var BaseCollection = Backbone.Collection.extend({
    initialize: function (options) {
      this.sandbox = options.sandbox || {};
    }
  });

  return BaseCollection;

});
