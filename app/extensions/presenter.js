define(function () {
  'use strict';

  var presenter = {
    el: '',

    // used by views for dynamically routing
    navigate: function (route) {
      return route;
    },

    // used by routers
    render: function (view, cb) {
      return cb(view);
    }
  };

  return {
    name: 'presenter',
    initialize: function (application) {
      application.sandbox.presenter = presenter;
      application.logger.log('Initializing extension: presenter');
    }
  };

});
