define(function () {
  'use strict';

  var LOADING_CONTENT = '<i class="fa fa-spinner fa-spin"></i>';
  var processNotifier = {
    notify: function (element) {
      // must be a jquery object
      var el = element;

      el.data('original-content', el.html());
      el.attr('disabled', 'disabled');
      el.html(LOADING_CONTENT);

    },

    stop: function (element) {
      element.html(element.data('original-content'));
      element.removeAttr('disabled');
      element.removeData('original-content');
    }
  };

  return {
    name: 'process-notifier',
    initialize: function (application) {

      application.sandbox.processNotifier = processNotifier;
      application.logger.log('Initializing extension: process-notifier');
    }
  };

});
