define(['htmlbeautify'], function (BeautifyHtml) {
  'use strict';

  return {
    name: 'formatter',
    initialize: function (application) {
      application.sandbox.formatter = {
        /*jshint camelcase: false*/
        formatHtml: BeautifyHtml.html_beautify
      };

      application.logger.log('Initializing extension: formatter');
    }
  };

});
