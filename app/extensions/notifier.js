define(['toastr'], function (toastr) {
  'use strict';

  return {
    name: 'notifier',
    initialize: function (application) {

			toastr.options = {
				'closeButton': true,
				'debug': false,
				'positionClass': 'toast-bottom-right',
				'onclick': null,
				'showDuration': '300',
				'hideDuration': '1000',
				'timeOut': '5000',
				'extendedTimeOut': '1000',
				'showEasing': 'swing',
				'hideEasing': 'linear',
				'showMethod': 'fadeIn',
				'hideMethod': 'fadeOut'
			};

      application.sandbox.notifier = toastr;

      application.logger.log('Initializing extension: notifier');
    }
  };

});
