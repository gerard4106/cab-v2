define(function () {
  'use strict';

  return {
    name: 'data-source-collection',
    initialize: function (application) {
      // Your brilliant code here!
      application.logger.log('Initializing extension: data-source-collection');
    }
  };

});
