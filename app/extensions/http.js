define(['jquery'], function ($) {

  'use strict';

  /*jshint validthis: true */

  /**
   * garguelles
   * http utility for ajax requests
  **/
  function http(options) {
    this.url = options.url || '';
    this.params = options.params || {};
    this.method = options.method || 'GET';
    this.contentType = options.contentType || 'application/json';
  }

  /**
   * get data as JSON
   * class method
   * param[url] = api endpoint
   * param[data] = object params
  **/
  http.prototype.getJSON = function () {
    return $.getJSON(this.url, this.data);
  };

  /**
   * execute a synchronous request based on the options passed in the constructor
   * instance method
  **/
  http.prototype.request = function () {
    throw new Error('not implemented');
  };

  /**
   * execute a async request based on the options passed in the contruxtor
   * instance method
  **/
  http.prototype.requestAsync = function () {
    throw new Error('not implemented');
  };


  return {
    name: 'http',
    initialize: function (application) {
      // Your brilliant code here!
      application.logger.log('Initializing extension: http');
      application.sandbox.http = http;
    }
  };

});
