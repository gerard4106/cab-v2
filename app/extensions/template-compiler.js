define(['handlebars'], function (Handlebars) {
  'use strict';

  /**
   * implement handlebars.compile
   * param[template] The html template
  **/


  return {
    name: 'template-compiler',
    initialize: function (application) {
      application.logger.log('Initializing extension: template-compiler');
      application.sandbox.compileTemplate = function (template) {
        return Handlebars.compile(template);
      };
    }
  };

});
