define(function () {
  'use strict';

  return {
    name: 'dispatcher',
    initialize: function (application) {
      // Your brilliant code here!
      application.logger.log('Initializing extension: dispatcher');
      application.sandbox.dispatcher = {
        undelegate: function () {
          application.sandbox.emit('undelegate');
        }
      };
    }
  };

});
