define(function () {
  'use strict';

  /**
   * Implements web storage and their polyfils
   * TODO: implement polyfills
  **/

  var webStorage = {
    sessionStorage: window.sessionStorage,
    localStorage: window.localStorage
  };


  return {
    name: 'web-storage',
    initialize: function (application) {
      // Your brilliant code here!
      application.logger.log('Initializing extension: web-storage');
      application.sandbox.webStorage = webStorage;
    }
  };

});
