define(function () {
  'use strict';

  var canvas = {
    ACTIVE : undefined,
    button : {
      TYPE : ['btn-primary', 'btn-default', 'btn-success', 'btn-danger', 'btn-warning', 'btn-info', 'btn-link'],
      SIZE : ['btn-lg', 'btn-md', 'btn-sm']
    }
  };

  return {
    name: 'canvas',
    initialize: function (application) {
      application.sandbox.canvas = canvas;
    }
  };

});
