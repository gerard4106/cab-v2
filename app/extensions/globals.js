define(function () {
  'use strict';

  var globals = {
    CONTROL: 'sample',
    selectors : {
      rightPanel: {
        HEADING: '#cab-panel-right .cab-panel-heading',
        CONTROLS: '#toolbox-controls',
        PROPERTIES: '#toolbox-properties',
        EVENTS: '#toolbox-events'
      },
      middle: {
        CANVAS: '#cab-panel'
      },
      leftPanel: {
        PROJECT: '#module-project',
        WIDGETS: '#module-widgets-library',
        VIEW: '#cab-panel-left .cab-panel-heading'
      }
    },

    // todo refactor this
    messages: {
      generic: {
        datasource: {
          create: {
            success: {
              TITLE: 'Success!',
              MESSAGE: 'Your data source has been added to the list.'
            }
          }
        },
				CREATE_WIDGET: {
					success: {
						title: 'Success!',
						message: 'Your widget has been created.'
					},
					error: {
						title: 'Error!',
						message: 'Error creating widget. Please contact Gerard Arguilles.'
					},
				},
        UPDATE_WIDGET: {
          success: {
            title: 'Success!',
            message: 'Your widget has been updated'
          }
        },
				SAVE_FILE: {
					success: {
						title: 'Success!',
						message: 'Your file has been saved.'
					},
					error: {
						title: 'Error!',
						message: 'Error saving file. Please contact Gerard Arguilles.'
					},
				},
				CREATE_MENU: {
					success: {
						title: 'Success!',
						message: 'Your menu has been created.'
					},
					error: {
						title: 'Error!',
						message: 'Error creating menu. Please contact Gerard Arguilles.'
					}
				},
        CREATE_EXTENSION: {
          success: {
            title: 'Success!',
            message: 'Your extension has been created.'
          },
          error: {
            title: 'Error!',
            message: 'Error creating menu. Please contact Gerard Abet Nicko or the late Aaron Castanos!'
          }
        }
			},
      dashboardNewProject: {
        INVALID_PROJECT_TYPE_ID: 'project type id is not valid'
      }
    },
    sampleWidgetAssets: {
      caption : 'AddToCart [Design]',
      channel : '8397800a-782e-410b-ad91-ec6f81fe41b8',
      channelDesc : 'AddToCart',
      content : '<div class=\'ck-modal\' i... </div>\n </div>\n</div>',
      filename : 'main.tpl.html',
      folder : 'templates',
      isComponent : false,
      key : '8397800a-782e-410b-ad91...es_main-tpl-html_design',
      tooltip : 'AddToCart\\templates\\main.tpl.html [Design]',
      widgetData : {
        classType : 'widget',
        contextType : 'folder',
        formId : 28,
        widgetId : 142
      }
    },
    projectType : {
      1 : 'widget',
      2 : 'control',
      3 : 'theme',
      4 : 'app',
      5 : 'website'
    },
    fileClassifications : {
      MENU : 'menu',
      WIDGET : 'widget',
      CORE : 'core'
    },
    fileTypes : {
      TEMPLATES : 'templates',
      VIEWS : 'views',
      ASSETS : 'assets',
      MODELS : 'models',
      COLLETIONS : 'collections'
    },
    SENTIENT_WEB_SERVER: 'http://localhost:5000/',
    //SENTIENT_WEB_SERVER: 'http://172.16.0.220:5000/',
    //local
    HOST: 'http://localhost:3000/api/'
    //HOST: 'http://172.16.0.220:3000/api/'
    //gerard mac
    //HOST: 'http://172.16.0.167:3000/api/'
    //gerard pc
    //HOST: 'http://172.16.1.33:3000/api/'
    //abet laptop ofc
    //HOST: 'http://172.16.0.220:3000/api/'
  };

  return {
    name: 'globals',
    initialize: function (application) {

      application.logger.log('Initializing extension: globals');
      application.sandbox.globals = globals;

    }

  };

});
